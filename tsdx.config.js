const utils = require('tsdx/dist/utils');
const { babelPluginTsdx } = require('tsdx/dist/babelPluginTsdx');
const core = require('@babel/core');
const replace = require('@rollup/plugin-replace');

const excludeBabelHelpers = (rollupConfig, rollupOptions = {}) => {
    /*
     * By default, TSDX is configured to not externalize the regenerator-runtime like
     * it would other external `node_modules` dependencies. This means if many Easel Stack
     * packages used async/await, each package would end up bundling its own copy of the
     * regenerator-runtime. To resolve this, we override TSDX's internal rollup `external`
     * property to use a function (conveniently provided by TSDX) that will properly
     * externalize everything including regenerator-runtime.
     */
    rollupConfig.external = utils.external;

    rollupConfig.plugins = rollupConfig.plugins.map(plugin => {
        /*
         * This fixes a warning produced by @rollup/plugin-replace
         * See https://github.com/formium/tsdx/issues/981
         */
        if (plugin.name === 'replace') {
            return replace({
                'process.env.NODE_ENV': JSON.stringify(rollupOptions.env),
                preventAssignment: true,
            });
        }

        /*
         * By default, TSDX sets babelHelpers to 'bundled'. This attempts to bundle
         * in babel helpers (e.g. regenerator runtime). Setting this value to 'runtime'
         * will instead make these polyfills/plugins be require() statements. This does
         * mean that these deps will need to be installed as dependencies of each
         * Easel Stack package
         */
        if (plugin.name === 'babel') {
            return babelPluginTsdx({
                // Below is copied from TSDX'S configuration for babel
                exclude: 'node_modules/**',
                extensions: [...core.DEFAULT_EXTENSIONS, 'ts', 'tsx'],
                passPerPreset: true,
                custom: {
                    targets: rollupOptions.target === 'node' ? { node: '10' } : undefined,
                    extractErrors: rollupOptions.extractErrors,
                    format: rollupOptions.format,
                },

                // This is the change that ensures we do not bundle the helpers into the package's bundle
                babelHelpers: 'runtime',
            });
        }
        return plugin;
    });

    return rollupConfig;
};

// Not transpiled with TypeScript or Babel, so use plain Es6/Node.js!
module.exports = {
    // This function will run for each entry/format/env combination
    rollup(rollupConfig, rollupOptions) {
        rollupConfig = excludeBabelHelpers(rollupConfig, rollupOptions);

        return rollupConfig;
    },
};
