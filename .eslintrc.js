const config = require('eslint-config-react-app');
const omit = require('lodash.omit');

/*
 * Lint warnings often go ignored. This function iterates over all the
 * rules defined in `eslint-config-react-app` and replaces all
 * rules defined as warnings with errors
 */
const rulesAsErrors = Object.keys(config.rules).reduce((acc, key) => {
    if (typeof config.rules[key] === 'string') {
        acc[key] = 'error';
    } else if (Array.isArray(config.rules[key]) && config.rules[key][0] === 'warn') {
        acc[key] = ['error', ...config.rules[key].slice(1)];
    } else {
        acc[key] = config.rules[key];
    }
    return acc;
}, {});

// no-unused-vars rule throws an error with eslint
const rules = omit(rulesAsErrors, 'no-unused-vars', 'no-undef');

module.exports = {
    plugins: ['lodash'],
    extends: [
        'react-app',
        'plugin:compat/recommended',
        'plugin:you-dont-need-lodash-underscore/all',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    globals: {
        __DEV__: 'readonly',
    },
    rules: {
        ...rules,
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': ['error'],
        /*
         * The default eslint-lint rule 'no-unused-expressions' gets tripped up on
         * Typescript optional call expressions. This change uses the
         * '@typescript-eslint/no-unused-expressions' in its place to solve this problem.
         * https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-expressions.md
         */
        'no-unused-expressions': 'off',
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/ban-types': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-unused-expressions': 'error',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-unused-vars': 'warn',
        // useAsyncEffect is a wrapper around useEffect that pipes through deps array
        'react-hooks/exhaustive-deps': [
            'error',
            {
                additionalHooks: '(useAsyncEffect)',
            },
        ],

        'react/jsx-no-constructed-context-values': 'error',
        'import/no-extraneous-dependencies': [
            'error',
            {
                // allows for dev dependencies to references in files that match these patterns
                devDependencies: [
                    '**/**/__tests__/**/*',
                    '**/**/__fixtures__/**/*',
                    '**/**/__mocks__/**/*',
                    '**/**/__stories__/**/*',
                    '**/@config/**/*',
                ],
                // ignores dependencies if they are listed as peer dependencies
                peerDependencies: true,
            },
        ],
        'import/named': 'error',
        'import/namespace': 'error',
        'import/default': 'error',
        'import/export': 'error',
        'import/no-named-as-default': 'error',
        'import/no-absolute-path': 'error',
        'import/no-dynamic-require': 'error',
        'import/no-cycle': 'error',
        'import/no-useless-path-segments': 'error',
        'import/no-duplicates': 'error',
        'lodash/import-scope': [2, 'method-package'], // prefer format lodash.uniqueid instead of lodash/uniqueid
        'no-restricted-imports': [
            'error',
            {
                paths: [
                    'jquery',
                    {
                        name: 'moment',
                        message: "Use date-fns or luxon instead because they're tree shakable",
                    },
                    {
                        name: 'underscore',
                        message:
                            'If you need to use underscore, please import the module needed with this pattern: `import map from "underscore/modules/map"` so it can be tree shaken',
                    },
                ],
            },
        ],
    },
};
