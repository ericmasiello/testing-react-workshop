# Testing React Workshop

This repository contains lessons on how to use Jest, React Testing Library, and other related tools and frameworks.

## Installation

```bash
yarn
```

## Running Tests

Tests are executed via `tsdx` but use Jest under the hood.

```bash
yarn test
```

If you wish to run tests in watch mode, use:

```bash
yarn test --watch
```

## How to use the source code

Each lesson has a `README.md` file. You should go through each lesson sequentially starting at `00-setup`. The end of each `README.md` has optional exercises that review lessons learned in that section. You are strongly encouraged to complete these lessons as they will help you understand the material.

## Need Help?

Feel free to slack me @Eric or email me emasiello@vistaprint.com with your questions.
