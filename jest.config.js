module.exports = {
    globals: {
        // __DEV__ is provided by tsdx.
        // See https://tsdx.io/optimization#advanced-code-babel-plugin-dev-expressions-code
        __DEV__: true,
    },
    coverageThreshold: {
        global: {
            branches: 100,
            functions: 100,
            lines: 100,
            statements: 100,
        },
    },
    collectCoverageFrom: [
        'src/**/*.{js,jsx,ts,tsx}',
        '!src/**/index.{js,jsx,ts,tsx}',
        '!src/__fixtures__/**/*',
        '!src/**/__stories__/**/*',
        '!src/@types/**/*',
        '!src/@config/**/*',
        '!src/**/*.stories.{js,jsx,ts,tsx}',
    ],
    moduleNameMapper: {
        // effectively ignores any css imports within modules
        '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
    },
    setupFilesAfterEnv: ['./src/@config/jest.ts'],
};
