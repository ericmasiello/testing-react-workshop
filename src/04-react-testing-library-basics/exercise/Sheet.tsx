import React from 'react';
import classNames from 'classnames';

type Props = React.HTMLAttributes<HTMLDivElement> & {
    fixed?: boolean;
};

export const Sheet: React.FC<Props> = props => {
    const { className, fixed = false, ...rest } = props;
    return (
        <div
            className={classNames(
                'sheet',
                {
                    'sheet--fixed': fixed,
                },
                className
            )}
            {...rest}
        />
    );
};
