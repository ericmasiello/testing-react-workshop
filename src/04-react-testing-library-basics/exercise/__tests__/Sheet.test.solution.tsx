import React from 'react';
import { render, screen } from '@testing-library/react';
import { Sheet } from '../Sheet';

// you should be able to pass a custom className prop and verify it is applied to the element
it('should apply a custom className', () => {
    render(<Sheet data-testid="sheet" className="test" />);
    const element = screen.getByTestId('sheet');

    expect(element).toHaveClass('test');
});

it('should render custom children or text', () => {
    render(<Sheet>Hello world</Sheet>);
    const element = screen.getByText(/Hello world/);

    expect(element).toHaveTextContent('Hello world');
});

// passing the fixed prop as true should apply a special className
it('should render with sheet--fixed className when fixed', () => {
    render(<Sheet data-testid="sheet" fixed />);
    const element = screen.getByTestId('sheet');

    expect(element).toHaveClass('sheet--fixed');
});

it('[BONUS] should render as a <div />', () => {
    render(<Sheet data-testid="sheet" />);
    const element = screen.getByTestId('sheet');

    expect(element.tagName).toEqual('DIV');
});

// consider trying to render the prop `data-foo="bar"`
it('[BONUS] should apply arbitrary props', () => {
    render(<Sheet data-testid="sheet" data-foo="bar" />);
    const element = screen.getByTestId('sheet');

    expect(element).toHaveAttribute('data-foo', 'bar');
});
