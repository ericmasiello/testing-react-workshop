import React from 'react';
import { render, screen } from '@testing-library/react';
import { Sheet } from '../Sheet';

// you should be able to pass a custom className prop and verify it is applied to the element
it.todo('should apply a custom className');

it.todo('should render custom children or text');

// passing the fixed prop as true should apply a special className
it.todo('should render with sheet--fixed className when fixed');

it.todo('[BONUS] should render as a <div />');

// consider trying to render the prop `data-foo="bar"`
it.todo('[BONUS] should apply arbitrary props');
