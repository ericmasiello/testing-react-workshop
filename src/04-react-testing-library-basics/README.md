# React Testing Library Basics

It’s important to understand when React Testing Library (RTL) renders your app, it's attempting to simulate rendering your components the same way a browser would. In your tests, you should:

1. Render your component
2. Interact with the output in the (JS)DOM using the methods provided by RTL
3. Make assertions based on the current state of the DOM

> We try to only expose methods and utilities that encourage you to write tests that closely resemble how your web pages are used.

Another way of thinking of it is this: if you were manually testing your component in the browser, what would you do?

1. You’d render the component to a webpage
2. Then (with your eyes) assert that things are rendered as you’d expect (e.g. “do you see a button that says ‘Submit’?”, etc).
3. Next, you might click on the button with the text “Submit” and verify the UI now appears in some new intended state.

Now imagine you can’t use your eyes to make the assertions. Instead, you need to use the browser’s console.

1. You’d need to write a bunch of `document.querySelector('…')` calls to assert various elements exist in the DOM.
2. Then you’ll use DOM API to interact with your UI
3. Maybe wait while some data is fetched
4. Then make additional assertions via the DOM that elements exist in the DOM in the way you’ve intended.

This visual-less based testing/assertions is conceputally what you're doing with RTL.

## JS DOM

It’s also important to note that because RTL is running in Jest (and not in the browser), there is no actual DOM. Instead, we rely on JS DOM. JS DOM is basically a giant polyfill for the entire DOM spec. It’s pretty incredible. However, there are certain things you just can’t assert with JS DOM since it doesn’t actually render anything to the screen. Here are a few examples:

1. There’s no way to assert CSS styles appear visually correct. You can assert that DOM elements have `className` values or `style` attributes set. But there’s no way to assert that the text is blue without either setting the `color` with `style.color = ‘blue’` or just trusting that your CSS that sets it blue is doing its job.
2. Similarly, DOM APIs that tell you information about the dimensions and position of an element are **minimally** mocked by JS DOM. For example, calling `element.getBoundingClientRect()` on an element will return you an object with all the properties you’d expect to see (`x`, `y`, `width`, `height`, `top`, `left`, `right`, `bottom`) but all the values will be `0`. JS DOM has no idea how big the element actually is so it just stubs out a return value.

There are other APIs built into the DOM that may need additional polyfilling or mocking. The `fetch` API should be polyfilled. I recommend `import 'jest-fetch-mock';`. Similarly, other APIs like `matchMedia` do not exist. You'll need to manually mock these. See https://jestjs.io/docs/manual-mocks#mocking-methods-which-are-not-implemented-in-jsdom.

## Basic Component Tests with RTL

> Based on the Guiding Principles, your test should resemble how users interact with your code (component, page, etc.) as much as possible.

https://testing-library.com/docs/guiding-principles/

React Testing library exposes a number of utilities like `screen.getBy*`, `screen.queryBy*`, and `screen.findBy*`. These functions are conveniences around existing DOM APIs that add a little extra React-specific magic (namely wrapping many of them with `act(() => {})` calls.

### Synchronous queries with RTL

https://testing-library.com/docs/queries/about/

**Note:** are additional queries in form of `findBy*` and a utility method called `waitFor()`. We'll talk more about those later.

> **getBy**...: Returns the matching node for a query, and throw a descriptive error if no elements match or if more than one match is found (use getAllBy instead if more than one element is expected).

> **queryBy**...: Returns the matching node for a query, and return null if no elements match. This is useful for asserting an element that is not present. Throws an error if more than one match is found (use queryAllBy instead if this is OK).

> **getAllBy**...: Returns an array of all matching nodes for a query, and throws an error if no elements match.

> **queryAllBy**...: Returns an array of all matching nodes for a query, and return an empty array ([]) if no elements match.

Generally, its preferred you use `getBy*` or `getByAll*` over the "query" variants. With the "get" variants, RTL will immediately fail if the element does not exist. When it fails, it will automatically print the current state of the DOM to the screen. This is very helpful when trying to debug an error.

```tsx
import { screen } from '@testing-library/react';

/*
 * NOTE: the JS DOM tree is close to empty if you do not render a component first.
 * It more or less resembles the following:
 
 <html>
	<body>
	</body>
 </html>
 */
it('should fail immediately', () => {
    // Jest will immediately fail and echo the current state of JS DOM
    screen.getByText('text that does not exist on the page');
});

it('should pass', () => {
    // this will return null
    const element = screen.queryByText('text that does not exist on the page');

    expect(element).toBeNull();
});
```

The "query" variants should be used primarily for testing that element does **not** exist in the DOM.

### Prioritizing Queries

Each query variant (e.g. `getBy*`, `queryBy*`, `findBy*`) has multiple ways of querying the DOM. Consistent with the RTL guiding principles, you should favor techniques that promote accessibility and techniques real users would use to understand a UI.

For example, users understand the instruction: "click on a button." However, unless they were to crack open the dev tools, asking them to click on an element with a specific `className` or `id` is not something most users can accomplish.

[RTL suggests you favor particular queries over others](https://testing-library.com/docs/queries/about/#priority).

#### Most accessible queries (#1)

-   `getByRole`: aligns with [accessibility tree role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques#roles)

-   `getByLabelText` : usefor for testing forms. This will select the associated form element based on the its corresponding `<label>` text.

    ```html
    <label for="name">Name</label>
    <input id="name" />

    <!-- screen.getByLabelText("Name") will return the `<input />` element -->
    ```

-   `getByPlaceholderText` : `getByLabelText` is _preferred_ but this is a reasonable fallback if a label is not present

    ```html
    <input placeholder="first name" />

    <!-- screen.getByPlaceholderText("first name") will return the `<input />` element -->
    ```

-   `getByText` : Useful for querying elements that are non interactive that contain text

-   `getByDisplayValue` : Useful for forms if you wish to query by the form field's current value

    ```html
    <input value="hello world" />
    ```

#### Semantic Queries (#2)

1. `getByAltText`
2. `getByTitle`

#### Finally (#3)

1. `getByTestId`

### Matching by text

```typescript
import { screen } from '@testing-library/react';
```

`screen` is pre-bound to the `document.body` so its a reasonable starting place to make query assertions from

Queries support `string`, `regex`, or a function

```typescript
// Matching a string:
screen.getByText('Hello World'); // full string match
screen.getByText('llo Worl', { exact: false }); // substring match
screen.getByText('hello world', { exact: false }); // ignore case

// Matching a regex:
screen.getByText(/World/); // substring match
screen.getByText(/world/i); // substring match, ignore case (I prefer this approach!)
screen.getByText(/^hello world$/i); // full string match, ignore case
screen.getByText(/Hello W?oRlD/i); // substring match, ignore case, searches for "hello world" or "hello orld"

// Matching with a custom function:
screen.getByText((content, element) => content.startsWith('Hello'));
```

### Debugging

JS DOM takes care to create the host environment (the stuff that comes from the browser). RTL will then mount your components into this mock DOM. You can see what the HTML looks like by using `screen.debug()`.

```tsx
import React from 'react';
import { render, screen } from '@testing-library/react';
import { Sheet } from '../Sheet';

it('should [do something useful]', () => {
    // render takes care to mount the component into JSDOM
    render(<Sheet />);

    screen.debug(); // prints out the current state of the DOM
});
```

`screen.debug()` pretty prints whatever is in your DOM at that moment. So you’ll see the `body` element along with whatever HTML your component(s) emits. This is a really useful technique when trying to make sense of why tests aren’t working. Also of note, if your tests fail when using one of RTL’s `screen.getBy*` or `screen.findBy*` methods, it’ll call `screen.debug()` automatically for you.

### Manual Queries

> On top of the queries provided by the testing library, you can use the regular querySelector DOM API to query elements. Note that using this as an escape hatch to query by class or id is not recommended because they are invisible to the user. Use a testid if you have to, to make your intention to fall back to non-semantic queries clear and establish a stable API contract in the HTML.

Particularly when testing code you do not have full control over (e.g. library code), you may need to assert against elements that do not provide accessible interfaces and do not easily allow you to pass down a `data-testid` value. In these cases, you technically _can_ use DOM based lookups (e.g. `element.querySelector()`, `element.getElementById()`, etc.). It is not recommended but sometimes you gotta do what you gotta do.

```tsx
import React from 'react';
import { render } from '@testing-library/react';

it('should allow regular DOM query selectors', () => {
    const { container } = render(<MyComponent />);

    // not recommended
    const foo = container.querySelector('[data-foo="bar"]');

    // assert element with data-foo="bar" attribute is rendered on screen
    expect(foo).toBeDefined();
});
```

## Writing Synchronous Tests

When I talk about _synchronous_ vs. _asynchronous_ tests in React, what I'm talking about are components that depend on some asynchronous event to occur before the UI is in a state we can make assertions against. Examples of asynchronous behavior include rerendering once data has been fetched from some remote resource, waiting on promise to resolve, or even a `setTimeout` callback.

### Exercise

Implement `04-react-testing-library-basics/exercise/__tests__/Sheet.test.ts`
