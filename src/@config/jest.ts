// Provides a set of custom jest matchers that you can use to extend jest
// See https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import 'jest-fetch-mock';
import { server } from './mswConfig';

// Establish API mocking before all tests.
beforeAll(() => server.listen());

// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => server.resetHandlers());

// Clean up after the tests are finished.
afterAll(() => server.close());
