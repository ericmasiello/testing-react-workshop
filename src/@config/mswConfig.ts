import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { RequestHandlersList } from 'msw/lib/types/setupWorker/glossary';

// These are static handlers that exist for all tests.
// Add handlers for mocking API responses in tests or local development
// See https://mswjs.io/docs/getting-started/mocks/rest-api

// e.g.
// import { rest } from 'msw';
// import { setupServer } from 'msw/node';
// import { RequestHandlersList } from 'msw/lib/types/setupWorker/glossary';

// const handlers: RequestHandlersList = [
//     rest.get('*/foo', (req, res, ctx) => {
//         return res(
//             ctx.status(200),
//             ctx.json({
//                 default: 'foo',
//             })
//         );
//     }),
//     rest.get('*/bar', (req, res, ctx) => {
//         return res(
//             ctx.status(200),
//             ctx.json({
//                 default: 'bar',
//             })
//         );
//     }),
// ];
const handlers: RequestHandlersList = [
    // add handlers to me similar to example above
];

export const server = setupServer(...handlers);

//   If you need to customize server responses on a per test basis, you can
//   create a function here or in your test files that augments the server
//   to provide a custom response.

// /* inside MyThing.test.ts */
// import { rest } from 'msw';
// import { server } from '../@config/mswConfig';

// it('should fetch my greeting', async () => {
//     server.use(
//         rest.get('*', (_, res, ctx) => {
//             return res(ctx.status(200), ctx.json({ greeting: 'hello world' }));
//         })
//     );

//     const response = await fetch('https://vistaprint.com');
//     const result = await response.json();

//     expect(result).toEqual({ greeting: 'hello world' });
// });
