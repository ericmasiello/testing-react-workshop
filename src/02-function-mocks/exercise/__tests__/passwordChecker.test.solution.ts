import { isGoodPassword } from '../../passwordChecker';

// silence the errors from appearing in the Jest console.
const errorSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

beforeEach(() => {
    errorSpy.mockClear();
});

it('should return false when provided a bad password', () => {
    // act
    const result = isGoodPassword('password');

    // assert
    expect(result).toBe(false);
});

it('should return true when provided a good password', () => {
    // act
    const result = isGoodPassword('a real good password');

    // assert
    expect(result).toBe(true);
});

it('should log an error when provided a bad password', () => {
    // act
    isGoodPassword('password');

    // assert
    expect(errorSpy).toBeCalledTimes(1);
    expect(errorSpy).toBeCalledWith('this is the worst password');
    // alternative
    //expect(errorSpy.mock.calls[0]).toMatchInlineSnapshot();
});

it('should not log errors when provided a good password', () => {
    // act
    isGoodPassword('a real good password');

    // assert
    expect(errorSpy).toBeCalledTimes(0);
});
