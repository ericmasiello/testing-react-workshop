# Function Mocking

-   The first rule of mocking: don't mock
    -   Realistically, I find _most_ function mocking to be acceptable
    -   Avoid mocking entire modules except in a few rare conditions (e.g. animation libraries)
-   Prefer integration tests over _pure_ unit tests
    -   Unit tests suggest you mock everything except the module you're testing. Don't do this ❌

## Jest's tools for Mocking

There are two primary types of mocks in Jest: function mocks and module mocks.

### Mocking functions

This is the type of mocking I find most useful

Jest Mock Functions: https://jestjs.io/docs/mock-function-api

-   `jest.fn()`

    -   Creates a Jest mock function that allows you to write assertions about how the function was or wasn't called

        -   number of times function was called
        -   Arguments passed to the function
        -   What the function returned on each call

    -   `jest.fn()` create's a function that returns `undefined` (can be overridden with an actual implementation)

    -   ```typescript
        it('should assert that jest.fn was called', () => {
            // arrange
            const fn = jest.fn(); // returns a mock function
    
            // act
            fn(); // execute the mock function
    
            // assert
            expect(fn).toBeCalled(); // checks that it is called at all, doesn't matter how many times
            expect(fn).toBeCalledTimes(1);
            expect(fn).toReturnWith(undefined);
    
            // act (again)
            fn(22, 'hello');
    
            // assert some more
            expect(fn).toBeCalledTimes(2); // now called twice
            expect(fn).toBeCalledWith(22, 'hello'); // second time called with specific args
        });
        ```

    -   A mock function has a `mocks` property containing useful properites:

        -   `calls` : a two dimensional array where the first dimension represents the call instance and the 2nd dimension is the argument passed to that call instance
        -   `results` a single dimension array representing the return value from each call instance
            -   Each value is an object with a `type` and `value` property.
            -   `type` typically will be `"return"` or `"throw"`. There's also `"incomplete"` but I've never seen/used this.
            -   `value` has the actual result.

        ```typescript
        it('should be able to make assertions based on the mocks property', () => {
            const fn = jest.fn((a: number, b: number) => a + b); // provide mock implementation
    
            fn(1, 2); // called once
            fn(2, 4); // called twice
    
            expect(fn.mock.calls[0]).toEqual([1, 2]); // args passed to first call
            expect(fn.mock.calls[1]).toEqual([2, 4]); // args passed to second call
    
            expect(fn.mock.results[0]).toEqual({ type: 'return', value: 3 }); // 1 + 2
            expect(fn.mock.results[1]).toEqual({ type: 'return', value: 6 }); // 2 + 4
        });
        ```

-   `jest.spyOn()`

    -   Great for observing side effects

    -   Allow you to convert **an existing method on an object** into a mock/spy

    -   Retains the original implementation by default but you can override it (try to avoid this when possible)
    
    -   ```typescript
        const passwordChecker = {
            isGoodPassword(password: string) {
                if (password === 'password') {
                    console.error('this is the worst password');
                    return false;
                }
                return true;
            },
        };
        
        it('should assert that jest.fn was called', () => {
            // arrange
            const errorSpy = jest.spyOn(console, 'error');
        
            // act
            const result = passwordChecker.isGoodPassword('password');
        
            // assert
            expect(errorSpy).toBeCalled();
            expect(errorSpy).toBeCalledWith('this is the worst password'); // assert how it was called
            expect(errorSpy).toReturnWith(undefined);
            expect(result).toBe(false);
        });
        ```

## Quiz

-   Reusing mock functions for each test

    ```typescript
    // Will these tests pass?
    
    const passwordChecker = {
        isGoodPassword(password: string) {
            if (password === 'password') {
                console.error('this is the worst password');
                return false;
            }
            return true;
        },
    };
    
    const errorSpy = jest.spyOn(console, 'error');
    
    it('should call console.error when passed a bad password', () => {
        // act
        const result = passwordChecker.isGoodPassword('password');
    
        // assert
        expect(errorSpy).toBeCalledTimes(1);
        expect(errorSpy).toBeCalledWith('this is the worst password'); // assert how it was called
        expect(result).toBe(false);
    });
    
    it('should not call console.error when passed a good password', () => {
        // act
        const result = passwordChecker.isGoodPassword('a real good password');
    
        // assert
        expect(errorSpy).toBeCalledTimes(0);
        expect(result).toBe(true);
    });
    ```

(scroll down for answer)
.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

No. They won't. It will fail on `expect(errorSpy).toBeCalledTimes(0);`

**Solution**: need to call `mockClear()` before each test runs

Add:

```typescript
beforeEach(() => {
    // resets the internal counter for the spy function
    errorSpy.mockClear();
});
```

`mockClear()` exists on any Jest mock function so it can also be called on `jest.fn()`

Silence `console.error` during a test:

```typescript
// silence the errors from appearing in the Jest console.
const errorSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

// mock a getter method
const mockUserAgent = jest.spyOn(global.navigator, 'userAgent', 'get').mockImplementation(() => 'jest/env');
```

## Summary

### What is it appropriate to mock?

-   Mock any HTTP requests (e.g. those coming from `fetch`, `axios`, a GraphQL request, etc.). However, it is better to use Mock Service Worker for this (see next section).
-   Spying on methods via `jest.spyOn` without a custom `.mockImplementation()` is always okay
-   Use `jest.fn()` to mock callback functions
-   Mock different browser APIs that JS DOM does not implement (e.g. `window.matchMedia('(max-width: 600px)')`)

## Exercise

Implement `02-function-mocks/exercise/__tests__/passwordChecker.test.ts`

Try to use watch mode and the filters (`"a"` and `"p"`) by running `yarn test --watch` or `npm test -- --watch`.
