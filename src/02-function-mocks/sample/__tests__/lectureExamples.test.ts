it('should assert that jest.fn was called', () => {
    // arrange
    const fn = jest.fn();

    // act
    fn();

    // assert
    expect(fn).toBeCalled(); // checks that it is called at all, doesn't matter how many times
    expect(fn).toBeCalledTimes(1);
    expect(fn).toReturnWith(undefined);

    // act (again)
    fn(22, 'hello');

    // assert some more
    expect(fn).toBeCalledTimes(2); // now callced twice
    expect(fn).toBeCalledWith(22, 'hello'); // second time called with specific args
});

it('should be able to make assertions based on the mocks property', () => {
    const fn = jest.fn((a: number, b: number) => a + b);

    fn(1, 2); // called once
    fn(2, 4); // called twice

    expect(fn.mock.calls[0]).toEqual([1, 2]); // args passed to first call
    expect(fn.mock.calls[1]).toEqual([2, 4]); // args passed to second call

    expect(fn).toBeCalledWith(1, 2);

    expect(fn.mock.results[0]).toEqual({ type: 'return', value: 3 }); // 1 + 2
    expect(fn.mock.results[1]).toEqual({ type: 'return', value: 6 }); // 2 + 4
});
it('should provide an implementation that can be called', () => {
    const fn = jest.fn().mockImplementation(() => 42);
    expect(fn()).toBe(42);
    expect(fn).toReturnWith(42);
});
