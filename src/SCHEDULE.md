# Day 1

| End Time | Time Allotted | Topic                  |
| -------- | ------------- | ---------------------- |
| 9:00     |               |                        |
| 9:05     | 5             | Why this course?       |
| 9:05     | 0             | Configuring Jest       |
| 9:20     | 15            | Structuring Tests      |
| 9:45     | 25            | Useful Assertions      |
| 9:50     | 5             | Break                  |
| 10:20    | 30            | Function Mocking       |
| 10:40    | 20            | Exercise               |
| 10:50    | 10            | Review                 |
| 11:10    | 20            | Mocking APIs with MSW  |
| 11:25    | 15            | Exercise               |
| 11:35    | 10            | Review                 |
| 11:40    | 5             | Break                  |
| 12:05    | 25            | Testing Library Basics |
| 12:20    | 15            | Exercise               |
| 12:30    | 10            | Review                 |

# Day 2

| End Time | Time Allotted | Topic                 |
| -------- | ------------- | --------------------- |
| 9:00     |               |                       |
| 9:20     | 20            | Event Handling        |
| 9:30     | 10            | Exercise              |
| 9:40     | 10            | Review                |
| 10:05    | 25            | Async with RTL        |
| 10:35    | 30            | Exercise              |
| 10:50    | 15            | Review                |
| 10:55    | 5             | Break                 |
| 11:20    | 25            | Testing Hooks         |
| 11:40    | 20            | Exercise              |
| 11:50    | 10            | Review                |
| 12:20    | 30            | Recommended Practices |
| 12:30    | 10            | Wrap up, Q&A          |
