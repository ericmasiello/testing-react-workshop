# Handle Events with React Testing Library

### Quick Aside: Using Typescript type assertions in tests ​(:fire: hot take)

When you make a call to `screen.getBy*()` it will return a value of type `HTMLElement`. (`screen.queryBy*()` returns `HTMLElement | null`). The important thing to note is that `HTMLElement` is the most generic form of an `HTMLElement`. If you query for elements that possess specific HTML properties such `value` on form elements, Typescript will error.

```tsx
it('should be an empty form field', () => {
    render(<Form />);

    const firstNameInputElement = screen.getByLabelText(/first name/i);

    // ❌ TS Error: Property 'value' does not exist on type 'HTMLElement'.ts(2339)
    expect(firstNameInputElement.value).toBe('');
});
```

You could handle this via "type narrowing" in Typescript:

```tsx
it('should be an empty form field', () => {
    render(<Form />);

    const firstNameInputElement = screen.getByLabelText(/first name/i);

    if (firstNameInputElement instanceof HTMLInputElement) {
        // use type narrowing...
        expect(firstNameInputElement.value).toBe('');
    }
});
```

Type narrowing is appropriate for application code. However, for test code, you should generally avoid anything that results in conditional logic in your tests. This specific test is bad because you may end up with a false positive because if `firstNameInputElement` is **not** an `instanceof HTMLInputElement`, your test will **always** pass because no assertion is ever made.

If you must use conditional logic in your tests, you can improve them by adding an assertion count at the top:

```tsx
it('should be an empty form field', () => {
    expect.assertions(1); // ← ensures at least one assertion is made. if not, test will fail

    render(<Form />);

    const firstNameInputElement = screen.getByLabelText(/first name/i);

    if (firstNameInputElement instanceof HTMLInputElement) {
        expect(firstNameInputElement.value).toBe('');
    }
});
```

In tests, I prefer "type assertions" (often referred to as "type casting") as I find them to be easier and more maintainable.

```tsx
it('should be an empty form field', () => {
    render(<Form />);

    // Use "as" keyword from Typescript
    const firstNameInputElement = screen.getByLabelText(/first name/i) as HTMLFormElement;

    expect(firstNameInputElement.value).toBe('');
});
```

## Working with Events

React Testing Library exports a top level function called `fireEvent`. However, they encourage you to use a separate library called `@testing-library/user-event`. `userEvent` more accurately simulates how browsers behave when firing events. It comes with a host of conveience methods and "special characters" for simulating different events like pressing the `{enter}` key, `{esc}` key, `{arrowLeft}`, `{ctrl}`, and many more.

-   [User Event API](https://testing-library.com/docs/ecosystem-user-event/)
-   [Fire Event API](https://testing-library.com/docs/dom-testing-library/api-events/)

### Comparing `fireEvent` to `userEvent`

`fireEvent` maps 1:1 with events in the DOM (e.g. `change`, `click`, etc) whereas `userEvent` is more _user_ focused (e.g. `type`, `selectOptions`, etc).

```tsx
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const DemoForm = () => {
    const [name, setName] = React.useState('');
    return (
        <form
            onSubmit={event => {
                event.preventDefault();
                console.log('submitted!');
            }}
        >
            <fieldset>
                <label htmlFor="name">Name</label>
                <input id="name" value={name} onChange={event => setName(event.target.value)} />
            </fieldset>
            <div>Hi {name}</div>
            <button type="submit">Submit</button>
        </form>
    );
};

const consoleLogSpy = jest.spyOn(console, 'log');

beforeEach(() => {
    consoleLogSpy.mockClear();
});

it('should log to the console when submitting the form (using fireEvent)', () => {
    render(<DemoForm />);

    // trigger the change event and mock the event.target.value
    fireEvent.change(screen.getByLabelText(/name/i), { target: { value: 'Jim' } });
    expect(screen.findByText(/hi jim/i)).toBeDefined();

    // click the submit button
    fireEvent.click(screen.getByRole('button'));

    // assert submit handler was called as expected
    expect(consoleLogSpy).toBeCalledTimes(1);
    expect(consoleLogSpy).toBeCalledWith('submitted!');
});
```

vs.

```tsx
it('should log to the console when submitting the form (using userEvent)', () => {
    render(<DemoForm />);

    const nameInput = screen.getByLabelText(/name/i)
    
    // "type" the value into the field
    userEvent.type(nameInput, 'Jim');
    expect(screen.findByText(/hi jim/i)).toBeDefined();

    // click the submit button
    userEvent.click(screen.getByRole('button'));

    // assert submit handler was called as expected
    expect(consoleLogSpy).toBeCalledTimes(1);
    expect(consoleLogSpy).toBeCalledWith('submitted!');
});
```

### Limitations with Mocking Events

One pain point I have found with RTL is mocking certain types of events can be downright challenging or in some cases not possible. RTL allows you to mock properties of the `target` property of the `event` object but nothing else. Depending on your use case, this may making testing some event handlers impossible. For example, if your code depends on mocking properties of `currentTarget` and not `target`, you might be stuck.

> For reference `target` is where the event handler is bound. `currentTarget` is the element that triggered the event. Sometimes these are the same thing. However, due to event bubbling, clicking on a child element of a parent element with a click handler will still trigger the parent element's click handler to fire. In thise case, `target` is the parent element with the click handler and `currentTarget` is the child element where actually clicked.

```tsx
const handleClick: React.MouseEventHandler<HTMLButtonElement> = event => {
    console.log('target', (event.target as HTMLButtonElement).getBoundingClientRect());
    console.log('currentTarget', event.currentTarget.getBoundingClientRect());
};

it('should work fine when firing the event on the currentTarget :(', () => {
    render(
        <button onClick={handleClick}>
            Click <span>me</span>
        </button>
    );

    // Click on the button itself. In this case currentTarget === target
    fireEvent.click(screen.getByRole('button'), {
        target: {
            getBoundingClientRect() {
                return { test: 'this can be mocked (Button)' };
            },
        },
    });
});

// above test logs:
// target { test: 'this can be mocked (Button)' }
// currentTarget { test: 'this can be mocked (Button)' }

it('should prove that mocking events is challenging... :(', () => {
    render(
        <button onClick={handleClick}>
            Click <span>me</span>
        </button>
    );

    // Click on the span inside the button
    // currentTarget === span
    // target === button
    fireEvent.click(screen.getByRole('button').querySelector('span')!, {
        target: {
            getBoundingClientRect() {
                return { test: 'this can be mocked (Button > Span)' };
            },
        },
      	// NOTE: my mock of currentTarget is completely ignored by RTL
        currentTarget: {
            getBoundingClientRect() {
                return { test: 'this cannot be mocked (Button > Span)' };
            },
        },
    });
});

// The above test logs:
// target { test: 'this can be mocked (Button > Span)' }
// currentTarget { bottom: 0, height: 0, left: 0, right: 0, top: 0, width: 0 } <--- NO GOOD
```

### Exercise

Implement `05-react-testing-library-events/exercise/__tests__/Form.test.tsx`
