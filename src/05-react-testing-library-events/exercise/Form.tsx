import React from 'react';

const colors = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];

type Props = {
    handleSubmit?: (values: Record<string, string>) => void;
    firstName?: string;
    lastName?: string;
    favoriteColor?: string;
};

export const Form = (props: Props) => {
    const handleSubmit: React.FormEventHandler<HTMLFormElement> = event => {
        event.preventDefault();
        const formData = new FormData(event.currentTarget);
        const values: Record<string, string> = {};

        formData.forEach((value, key) => {
            values[key] = value as string;
        });

        props.handleSubmit?.(values);
    };
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="firstName">First Name</label>{' '}
                <input name="firstName" id="firstName" defaultValue={props.firstName} />
            </div>
            <div>
                <label htmlFor="lastName">Last Name</label>{' '}
                <input name="lastName" id="lastName" defaultValue={props.lastName} />
            </div>
            <div>
                <label htmlFor="favoriteColor">Favorite Color</label>{' '}
                <select name="favoriteColor" id="favoriteColor" defaultValue={props.favoriteColor}>
                    <option />
                    {colors.map(color => (
                        <option key={color} value={color}>
                            {color}
                        </option>
                    ))}
                </select>
            </div>
            <button type="submit">Submit</button>
        </form>
    );
};
