import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Form } from '../Form';

// NOTE: this does not require using fireEvent or userEvent
it.todo('should set the first name when passed an initial first name value');

// NOTE: this does not require using fireEvent or userEvent
it.todo('should ignore invalid color values');

// Use userEvent to simulate the different user events
it.todo('should call the the handleSubmit callback once submitted with the values in the form');
