import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Form } from '../Form';

it('should set the first name when passed an initial first name value', () => {
    render(<Form firstName="Yo Jimbo" />);

    expect((screen.getByLabelText(/first name/i) as HTMLInputElement).value).toBe('Yo Jimbo');
});

it('should ignore invalid color values passed as a prop', () => {
    render(<Form favoriteColor="i am not a valid color" />);

    const favorieColorSelect = screen.getByLabelText(/favorite color/i) as HTMLSelectElement;

    expect(favorieColorSelect.value).toBe('');
});

it('should call the the handleSubmit callback once submitted with the values in the form', async () => {
    const handleSubmit = jest.fn();

    render(<Form handleSubmit={handleSubmit} />);

    // set the form fields
    await userEvent.type(screen.getByLabelText(/first name/i), 'Steve');
    await userEvent.type(screen.getByLabelText(/last name/i), 'Stevenson');
    await userEvent.selectOptions(screen.getByLabelText(/favorite color/i), ['orange']);

    // submit the form
    await userEvent.click(screen.getByText(/submit/i));

    expect(handleSubmit).toBeCalledWith({
        firstName: 'Steve',
        lastName: 'Stevenson',
        favoriteColor: 'orange',
    });
});
