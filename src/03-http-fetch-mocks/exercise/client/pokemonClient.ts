type Resource = {
    name: string;
    url: string;
};

type Pokemon = {
    id: number;
    name: string;
    order: number;
    species: Resource;
    stats: {
        base_stat: number;
        effort: number;
        state: Resource;
    }[];
    types: {
        slot: number;
        type: Resource;
    }[];
    is_default: boolean;
    location_area_encounters: string;
    abilities: {
        ability: Resource;
        is_hidden: boolean;
        slot: number;
    }[];
    base_experience: number;
    forms: Resource[];
    game_indices: {
        game_index: number;
        version: Resource[];
    }[];
    height: number;
    weight: number;
    held_items: {
        item: Resource;
        version_details: {
            rarity: number;
            version: Resource;
        }[];
    }[];
    moves: {
        move: Resource;
        version_group_details: {
            level_learned_at: number;
            move_learn_method: Resource;
            version_group: Resource;
        }[];
    }[];
};

export const pokemonClient = async (pokemonName: string) => {
    if (!pokemonName) {
        throw new Error('You must provide a Pokemon name.');
    }
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName.toLowerCase().trim()}`);

    if (!response.ok) {
        throw new Error(`Request failed with status ${response.status}`);
    }

    const pokemon: Pokemon = await response.json();

    return pokemon;
};
