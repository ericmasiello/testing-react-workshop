import { rest } from 'msw';
import { server } from '../../../@config/mswConfig.solution.03';
import { fetchPokemonSize, fetchPokemonAbilities } from '../pokemonHelpers';

/*
 * Before you get started, you'll need to set up MSW to mock the pokemon api. 
 * 1. Open up src/@config/mswConfig.ts
 * 2. Import the following code:
  
    import { setupServer } from 'msw/node';
    import { rest } from 'msw';
    import { RequestHandlersList } from 'msw/lib/types/setupWorker/glossary';
    import pikachu from '../03-http-fetch-mocks/exercise/__fixtures__/pikachu.json';

 * 3. Follow the directions for configuring a global handler that will mock calls
 *    to the pokemon API and return the pikachu fixture. You will need to look at
 *    03-http-fetch-mocks/exercise/client/pokemonClient.ts to understand what url
 *    pattern to pass to your MSW handler.
 */

describe('fetchPokemonSize', () => {
    it('should return the pokemon height and weight as numbers', async () => {
        expect.assertions(1);

        const result = await fetchPokemonSize('somePokemon');

        expect(result).toMatchObject({
            height: expect.any(Number),
            weight: expect.any(Number),
        });
    });

    it('should return the pokemon height and weight as numbers (option 2)', async () => {
        expect.assertions(1);

        await expect(fetchPokemonSize('somePokemon')).resolves.toMatchObject({
            height: expect.any(Number),
            weight: expect.any(Number),
        });
    });

    // 💁‍♀️ you'll need to insert a custom "inline" MSW response for these
    it('should throw an error when the API is down', async () => {
        expect.assertions(1);

        server.use(
            rest.get('https://pokeapi.co/api/v2/pokemon/*', (_, res, ctx) => {
                return res(ctx.status(500));
            })
        );

        await expect(fetchPokemonSize('whatever')).rejects.toThrow();
    });

    it('should throw an error when you pass in an unknown pokemon', async () => {
        expect.assertions(1);

        server.use(
            rest.get('https://pokeapi.co/api/v2/pokemon/*', (_, res, ctx) => {
                return res(ctx.status(404));
            })
        );

        await expect(fetchPokemonSize('unknown pokemon')).rejects.toThrow();
    });
});

describe('fetchPokemonAbilities', () => {
    // 💁‍♀️ consider using snapshot testing here https://jestjs.io/docs/snapshot-testing
    it('should fetch the pokemons abilities', async () => {
        expect.assertions(1);

        await expect(fetchPokemonAbilities('somePokemon')).resolves.toMatchInlineSnapshot(`
                    Array [
                      "static",
                      "lightning-rod",
                    ]
                `);
    });

    it('should return an empty list when given an unknown pokemon', async () => {
        expect.assertions(1);

        server.use(
            rest.get('https://pokeapi.co/api/v2/pokemon/*', (_, res, ctx) => {
                return res(ctx.status(404));
            })
        );

        await expect(fetchPokemonAbilities('somePokemon')).resolves.toEqual([]);
    });
});
