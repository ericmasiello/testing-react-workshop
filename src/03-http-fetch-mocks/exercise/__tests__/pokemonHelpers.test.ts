import { fetchPokemonSize, fetchPokemonAbilities } from '../pokemonHelpers';

/*
 * Before you get started, you'll need to set up MSW to mock the pokemon api. 
 * 1. Open up src/@config/mswConfig.ts
 * 2. Import the following code:
  
    import { setupServer } from 'msw/node';
    import { rest } from 'msw';
    import { RequestHandlersList } from 'msw/lib/types/setupWorker/glossary';
    import pikachu from '../03-http-fetch-mocks/exercise/__fixtures__/pikachu.json';

 * 3. Follow the directions for configuring a global handler that will mock calls
 *    to the pokemon API and return the pikachu fixture. You will need to look at
 *    03-http-fetch-mocks/exercise/client/pokemonClient.ts to understand what url
 *    pattern to pass to your MSW handler.
 */

describe('fetchPokemonSize', () => {
    it.todo('should return the pokemon height and weight as numbers');

    // 💁‍♀️ you'll need to insert a custom "inline" MSW response for these
    it.todo('should throw an error when the API is down');

    it.todo('should throw an error when you pass in an unknown pokemon');
});

describe('fetchPokemonAbilities', () => {
    // 💁‍♀️ consider using snapshot testing here https://jestjs.io/docs/snapshot-testing
    it.todo('should fetch the pokemons abilities');

    it.todo('should return an empty list when given an unknown pokemon');
});
