import { pokemonClient } from './client/pokemonClient';

// given a valid pokemon, this function returns their height and weight { height: number, weight: number }
export const fetchPokemonSize = async (pokemonName: string) => {
    const pokemonData = await pokemonClient(pokemonName);

    return {
        height: pokemonData.height,
        weight: pokemonData.weight,
    };
};

// given a valid pokemon, this function returns their abilities as a flattened array
export const fetchPokemonAbilities = async (pokemonName: string) => {
    try {
        const pokemonData = await pokemonClient(pokemonName);
        return pokemonData.abilities.map(({ ability }) => ability.name);
    } catch (error) {
        return [];
    }
};
