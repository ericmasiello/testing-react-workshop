# Techniques for Mocking API Requests

## Why mock

When dealing with lower level tests (i.e. unit, integration), you want your tests to be fast and reliable. Test "flakiness" erodes
confidence in your tests. For this reason, I always recommend mocking any http requests.

## How to mock API calls

You can mock an API client a number of ways.

### Mock the client wrapper via `jest.mock` (Not recommended)

```tsx
// Source https://kentcdodds.com/blog/stop-mocking-fetch
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { client } from '../../utils/api-client'; // <-- client is now a Jest mock function

jest.mock('../../utils/api-client');

it('clicking "confirm" submits payment', async () => {
    const shoppingCart = buildShoppingCart();
    render(<Checkout shoppingCart={shoppingCart} />);

    // mock the client to return a promise with the value { success: true }
    (client as jest.Mock).mockResolvedValueOnce(() => ({ success: true })); // <-- note the usage of jest.fn mock methods

    // click the confirm button on the UI
    userEvent.click(screen.getByRole('button', { name: /confirm/i }));

    // verify the client was called with appropriate data
    expect(client).toHaveBeenCalledWith('checkout', { data: shoppingCart });
    expect(client).toHaveBeenCalledTimes(1);

    // verify the success UI appears
    expect(await screen.findByText(/success/i)).toBeInTheDocument();
});
```

#### `jest.mock`

`jest.mock` allows you to mock individual modules either from your `node_modules` directory or modules within your source code.

**Note:** `jest.mock` can be used to mock any module. There's nothing to say you can only use this on api clients.

-   Every function that is exported via `export` or `default export` will be turned into a Jest mock function (i.e. `jest.fn`).

-   Any exported static values (e.g. `export const foo = 'bar'`) are left alone

-   You don't need to worry about calling `jest.mock()` before your `import` statements. Jest automatically hoists all `jest.mock` calls to the top of your module.

-   You can optionally supply a custom mock implementation as a the second argument

    ```tsx
    // client will be mocked based on the implementation below
    import { client } from '../../utils/api-client';
    
    // create the mock
    jest.mock('../../utils/api-client', () => {
      // this bypasses the mocking and loads the ACTUAL module
      const actual = jest.requireActual('../../utils/api-client');
    
      // since we are only interested in mocking the client export,
      // we provide a custom mocked client but we re-export any other
      // remaining exports from `api-client`
      return {
        ...actual,
        client() {
          return Promise.resolve({ result: 'mock result' }),
        }
      }
    });
    ```

One small inconvenience when using mocks this way is that Typescript has no one way of knowing that `client` function you imported is now actually a Jest mock function. This means you'll need to do a type assertion (using the `as` keyword).

```typescript
import { client } from '../../utils/api-client';

// Note that its OK that we mock the client AFTER we import it
// Jest automatically hoists these jest.mock() calls above our imports
jest.mock('../../utils/api-client');

it('...', () => {
    // providing the generic args for `jest.Mock`
    (client as jest.Mock<ReturnType<typeof client>, Parameters<typeof client>>).mockResolvedValueOnce(() => ({
        success: true,
    }));

    // or just avoid the generic arguments if you don't care
    (client as jest.Mock).mockResolvedValueOnce(() => ({ success: true }));
    // ...
});
```

### Mock the `fetch` API itself via a `jest.spyOn` (Not recommended)

```tsx
// Source https://kentcdodds.com/blog/stop-mocking-fetch
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

beforeAll(() => jest.spyOn(window, 'fetch'));
// assuming jest's resetMocks is configured to "true" so
// we don't need to worry about cleanup
// this also assumes that you've loaded a fetch polyfill like `whatwg-fetch`

it('clicking "confirm" submits payment', async () => {
    const shoppingCart = buildShoppingCart();
    render(<Checkout shoppingCart={shoppingCart} />);

    // mock the return value of `window.fetch`
    (window.fetch as jest.Mock).mockResolvedValueOnce({
        ok: true,
        json: async () => ({ success: true }),
    });

    // click on the confirm button
    userEvent.click(screen.getByRole('button', { name: /confirm/i }));

    // now verify that `fetch()` was called as expected
    expect(window.fetch).toHaveBeenCalledWith(
        '/checkout',
        expect.objectContaining({
            method: 'POST',
            body: JSON.stringify(shoppingCart),
        })
    );
    expect(window.fetch).toHaveBeenCalledTimes(1);
    expect(await screen.findByText(/success/i)).toBeInTheDocument();
});
```

## Which is better? What's the trade off?

-   Mocking the client via `jest.mock` means we may end up using the client incorrectly in our `Checkout` component. What if the parameters to the `client` changed at some point? Because we've mocked the entire function, there's nothing to prove our `Checkout` component is using the client function correctly.
-   Mocking/spying on fetch is better as it puts us a bit closer to the _metal_. The problem, however, is that mocking `fetch` is difficult to scale across your entire test suite. You in effect end up "reimplementing" your back-end throughout all your tests.
-   Also, what if you're not using `fetch`? Maybe you're using a library that uses `XMLHttpRequest`. Maybe that's what something like `apollo` or `axios` use?

It would be _best_ if we could avoid mocking our client but still have the benefit of stable responses that do not depend on an external service.

# Enter Mock Service Worker (MSW) (Recommended)

MSW acts as a proxy between the client (browser or node environment) and any external API requests. If you use it in a browser environment (which Jest is not), it uses an actual service worker. In a Node environment, it creates what effectively acts like a service worker.

## Creating MSW Handlers

Import `rest` from `msw` and `setupServer` from `msw/node`. `rest` works similarly to Express, allowing you to define various HTTP handlers (e.g. `get`, `post`, etc). Pass your array of handlers to `setupServer()` and then call `server.listen()` before running all your tests.

```typescript
// credit https://kentcdodds.com/blog/stop-mocking-fetch
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import * as users from './users';

const handlers = [
    rest.get('/login', async (req, res, ctx) => {
        const user = await users.login(JSON.parse(req.body));
        return res(ctx.json({ user }));
    }),
    rest.post('/checkout', async (req, res, ctx) => {
        const user = await users.login(JSON.parse(req.body));
        const isAuthorized = user.authorize(req.headers.Authorization);
        if (!isAuthorized) {
            return res(ctx.status(401), ctx.json({ message: 'Not authorized' }));
        }
        const shoppingCart = JSON.parse(req.body);
        // do whatever other things you need to do with this shopping cart
        return res(ctx.json({ success: true }));
    }),
];

const server = setupServer(...handlers);

// set up MSW to run before all your tests execute in Jest
beforeAll(() => server.listen());
// if you need to add a handler after calling setupServer for some specific test
// this will remove that handler for the rest of them
// (which is important for test isolation):
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
```

Then just write your test and stop worry about how to mock fetch

```typescript
// Source https://kentcdodds.com/blog/stop-mocking-fetch
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

it('clicking "confirm" submits payment', async () => {
    const shoppingCart = buildShoppingCart();
    render(<Checkout shoppingCart={shoppingCart} />);

    userEvent.click(screen.getByRole('button', { name: /confirm/i }));
    expect(await screen.findByText(/success/i)).toBeInTheDocument();
});
```

## MSW: Global Handlers vs. Run Time Handlers

-   MSW used with Jest supports the idea of "global handlers" and then test-specific handlers
-   Global handlers are appropriate for mocking all the happy paths
-   Test-Specific handlers are great for testing edge cases and errors
-   Example:
    -   Configure our global handlers https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/@config/mswConfig.ts
    -   Configure Jest to run MSW with our global handlers https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/@config/jest.ts
        -   Note: its also a good idea to mock `fetch` using something like `jest-fetch-mock`
    -   Happy path tests use rely on the globally defined handlers https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L90-113
    -   We then handle edge cases using custom handlers on a per test basis https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L208-212
        -   These handlers only "live" for the lifespan of that particular `test`/`it` function so there's no need to clean up of the test handlers you create test to test

## Does this mean I never need to spy/mock a `fetch` client?

No. But it does mean you'll rarely have to do it.

Example: I am testing a component that relies on a multiple requests. Maybe I want the first one to complete and then i want to trigger some user behavior that occurs while the second request is in flight such as unmounting a component. See https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L484

1. This test still uses MSW to mock the responses
2. We want to trigger a rerender of the component after the first request succeeds and while the second request is in flight. To do this we should _spy_ on both clients. This allows them to operate as normal but still allows us to listen to what they are doing.
3. Create the two spies (one for each client) https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L10-11
4. Clean up after each test in a `beforeEach` https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L22-23
5. Render the component (this triggers the API requests) https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L470-481
6. Specifically wait for the first request to complete via the spy https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L484
7. Trigger a rerender of the component while the second request is in flight https://gitlab.com/vistaprint-org/design-technology/easel-stack/-/blob/main/packages/design-views-provider-react/src/__tests__/DesignViewsProvider.test.tsx#L487-498

# Resources

-   https://kentcdodds.com/blog/stop-mocking-fetch
-   https://jestjs.io/docs/tutorial-async

### Exercise

Implement `03-http-fetch-mocks/exercise/__tests__/pokemonHelpers.test.ts`
