import { renderHook } from '@testing-library/react-hooks';
import { DocumentProvider, useDocument } from '../DocumentProvider';
import cimDoc from '../__fixtures__/cimdoc';
import { rest } from 'msw';
import { server } from '../../../@config/mswConfig.solution.06'; // TODO: replace me with regular mswConfig import

/*
 * NOTE: Use the existing mswConfig from the previous section
 */

/*
 * TODO: spyOn console.error, providing it a mock implementation that does nothing to silence the errors
 */
const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

/*
 * TODO: reset the the console error spy before each test case runs
 */
beforeEach(() => {
    consoleErrorSpy.mockClear();
});

it('should provide the state as uninitialized when no CimDoc or docId is provided', () => {
    const { result } = renderHook(() => useDocument(), { wrapper: DocumentProvider });

    expect(result.current.state).toBe('UNINITIALIZED');
});

it('should immediately return the cimDoc in a ready state with the cimDoc when passed as a prop', () => {
    const { result } = renderHook(() => useDocument(), { wrapper: DocumentProvider, initialProps: { cimDoc } });

    expect(result.current.state).toBe('READY');
});

it('should go into a loading state when fetching the cimDoc', () => {
    const { result } = renderHook(() => useDocument(), { wrapper: DocumentProvider, initialProps: { docId: 'abc' } });

    expect(result.current.state).toBe('LOADING');
});

it('should fetch the document after loading', async () => {
    const { result, waitFor } = renderHook(() => useDocument(), {
        wrapper: DocumentProvider,
        initialProps: { docId: 'abc' },
    });

    // initially in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for data to be ready
    await waitFor(() => result.current.state === 'READY');

    // assert that cimDoc is no longer null
    expect(result.current.cimDoc).not.toBeNull();
    // assert there are no errors
    expect(result.current.errorMessage).toBeUndefined();
});

/*
 * NOTE: this test requires you mock the response from https://storage.documents.cimpress.io/v3/documents/*
 * so that you can test the error case
 */
it('should set the error state when failing to fetch the document', async () => {
    server.use(
        rest.get('https://storage.documents.cimpress.io/v3/documents/*', (req, res, ctx) => {
            return res(ctx.status(404));
        })
    );

    const { result, waitFor } = renderHook(() => useDocument(), {
        wrapper: DocumentProvider,
        initialProps: { docId: 'abc' },
    });

    // initially in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for data to be in an error state
    await waitFor(() => result.current.state === 'ERROR');

    // assert there is no cimDoc
    expect(result.current.cimDoc).toBeNull();

    // assert there is an error message
    expect(result.current.errorMessage).toMatchInlineSnapshot(
        `"Failed to fetch document with docId abc. Error status: 404"`
    );
});

it('should log an error when failing to fetch a document', async () => {
    server.use(
        rest.get('https://storage.documents.cimpress.io/v3/documents/*', (req, res, ctx) => {
            return res(ctx.status(404));
        })
    );

    const { result, waitFor } = renderHook(() => useDocument(), {
        wrapper: DocumentProvider,
        initialProps: { docId: 'abc' },
    });

    // initially in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for data to be in an error state
    await waitFor(() => result.current.state === 'ERROR');

    expect(consoleErrorSpy.mock.calls[0][0]).toMatchInlineSnapshot(
        `[Error: Failed to fetch document with docId abc. Error status: 404]`
    );
});
