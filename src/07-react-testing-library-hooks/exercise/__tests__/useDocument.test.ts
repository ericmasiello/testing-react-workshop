import { renderHook } from '@testing-library/react-hooks';
import { DocumentProvider, useDocument } from '../DocumentProvider';
import cimDoc from '../__fixtures__/cimdoc';
import { rest } from 'msw';
import { server } from '../../../@config/mswConfig';

/*
 * NOTE: Use the existing mswConfig from the previous section
 */

/*
 * TODO: spyOn console.error, providing it a mock implementation that does nothing to silence the errors
 */

/*
 * TODO: reset the the console error spy before each test case runs
 */

it.todo('should provide the state as uninitialized when no CimDoc or docId is provided');

it.todo('should immediately return the cimDoc in a ready state with the cimDoc when passed as a prop');

it.todo('should go into a loading state when fetching the cimDoc');

it.todo('should fetch the document after loading');

/*
 * NOTE: this test requires you mock the response from https://storage.documents.cimpress.io/v3/documents/*
 * so that you can test the error case
 */
it.todo('should set the error state when failing to fetch the document');

it.todo('should log an error when failing to fetch a document');
