export const fetchDocument = async (docId: string, abortSignal?: AbortSignal) => {
    try {
        const response = await fetch(`https://storage.documents.cimpress.io/v3/documents/${docId}`, {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            signal: abortSignal,
        });

        if (!response.ok) {
            throw new Error(`Failed to fetch document with docId ${docId}. Error status: ${response.status}`);
        }
        const data = await response.json();

        return data;
    } catch (error) {
        // rethrow errors that do not come from an abort controller cancelling the request
        if (error.name !== 'AbortError') {
            throw error;
        }
    }
};
