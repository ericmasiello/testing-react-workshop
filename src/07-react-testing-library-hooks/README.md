# Testing Hooks

## The problem

You'll observe that in previous exercise, we were primarily focused on testing the effect of a hook, not the React component itself. To solve this, we had to wrap our hook inside a custom `DocumentChild` component. This technique may still be necessary for other types of tests from time to time. But in this particular case, we were mostly interested in _data_ the hook exposes through the provider component.

This is the perfect use case for `@testing-library/react-hooks`. This library automatically wraps your hook in a component and exposes the return value of the hook to you in your test along with a few test utilities.

## The API

https://react-hooks-testing-library.com/

```ts
import { renderHook } from '@testing-library/react-hooks';
import { useToggleGetter } from '../useToggleGetter';

it('should do ...', () => {
    const { result, rerender, unmount, waitForNextUpdate, waitForValueToChange } = renderHook(() =>
        useToggleGetter('some value')
    );
});
```

There some additional options you can pass `renderHook`. The most important of which is `wrapper`. This is particularly helpful when using context within your hook.

```ts
const ToggleProvider = () => {
    /* ... */
};

const useToggleSelection = () => {
    const selection = useContext(ToggleSelectionContext);
    if (selection === undefined) {
        throw new Error('Component must be wrapped in a ToggleProvider');
    }

    return selection;
};

renderHook(() => useToggleSelection(), { wrapper: ToggleProvider });
```

The component that wraps the hook you are testing (`useToggleSelection()`), is rendered as `children` of the `wrapper` component.

If you want to pass in custom props to the `wrapper` component, set the `initialProps` property:

```ts
renderHook(() => useToggleSelection(), { wrapper: ToggleProvider, initialProps: { foo: 'foo', bar: 'bar' } });

/*
 *  <ToggleProvider foo="foo" bar="bar">
 *      <ComponentCreatedByRenderHook />
 *  </ToggleProvider>
 */
```

If you need to render more than just a single wrapper component, change your test to be a `*.tsx` file and you can write your test like so:

```tsx
const OtherProvider = () => {
    /* ... */
};

const ToggleProvider = () => {
    /* ... */
};

const useToggleSelection = () => {
    const selection = useContext(ToggleSelectionContext);
    if (selection === undefined) {
        throw new Error('Component must be wrapped in a ToggleProvider');
    }

    return selection;
};

renderHook(() => useToggleSelection(), {
    wrapper: props => {
        const { children, ...otherProps } = props;
        return (
            <OtherProvider {...otherProps}>
                {/* the component created by renderHook IS children */}
                <ToggleProvider>{children}</ToggleProvider>
            </OtherProvider>
        );
    },
    initialProps: { other1: 'foo', other2: 'bar' },
});
```

### `result`

This is where you'll spend most of your time. `result` is an object that contains 3 properties:

```ts
{
  all: Array<any>
  current: any,
  error: Error
}
```

`current` represents the latest value returned from your hook.

```ts
const useMyHook = () => {
    return 'foo'; // <-- 'foo' === result.current
};
```

Of note, this library not only wraps your hook inside a component but it also wraps it inside a React error boundary. This allows the `renderHook()` to expose any errors throw during rendering as a property: `result.error`.

```ts
const useMyHook = () => {
    throw new Error('I am an error');
};

const { result } = renderHook(() => useMyHook());

expect(result.error).toBeDefined();
```

### `rerender`

```ts
const { rerender, result } = renderHook(() => useMyHook(), {
    wrapper: MyWrapper,
    initialProps: {
        /* some initial props */
    },
});

expect(result.current).toEqual(/* something initially */);

rerender({
    /* ...newProps */
});

expect(result.current).toEqual(/* something else */);
```

### `unmount`

This is useful if you want to test for behavior when the hook is unmounted, typically in the return function of a `useEffect`

```ts
export const usePreventZoomGesture = () => {
    useEffect(() => {
        // See https://use-gesture.netlify.app/docs/hooks/#about-the-pinch-gesture
        const preventGesture = (event: Event) => {
            event.preventDefault();
        };
        document.addEventListener('gesturestart', preventGesture);
        document.addEventListener('gesturechange', preventGesture);

        return () => {
            document.removeEventListener('gesturestart', preventGesture);
            document.removeEventListener('gesturechange', preventGesture);
        };
    }, []);
};

// tests...
const removeEventListenerSpy = jest.spyOn(document, 'removeEventListener');

it('should remove gesturestart and gesturechange event listeners when unmounted', () => {
    const { unmount } = renderHook(() => usePreventZoomGesture());

    unmount();

    expect(removeEventListenerSpy).toBeCalledTimes(2);
    expect(removeEventListenerSpy.mock.calls[0][0]).toBe('gesturestart');
    expect(removeEventListenerSpy.mock.calls[1][0]).toBe('gesturechange');
});
```

### `waitForNextUpdate` and `waitFor`

These are conceptually similar to the `waitFor` exposed by RTL. `waitForNextUpdate` is simply that, it allows you to perform some async behavior, _wait for the next update_, and then make some assertion afterward.

```ts
it('should do async stuff', async () => {
    const { waitForNextUpdate, result } = renderHook(() => useAsyncCounter());

    // verify it is intially 0
    expect(result.current.value).toBe(0);

    result.current.asyncIncrement();

    await waitForNextUpdate();

    // verify value is now 1
    expect(result.current.value).toBe(1);
});
```

`waitForNextUpdate` literally waits for the next update and then resolves. Sometimes the timing of async behavior may race with other updates and you may end up in conditions where sometimes _the next update_ is not yet the value you want. For this reason, I prefer to use `waitFor`.

 `waitFor` works similarly to `waitFor()` in the RTL world. The main difference is the function you pass it is treated as a selector. `waitFor` continues to poll the callback selector function (re-executing it over and over) until it returns true or undefined without throwing an exception. It is typical that you use `result.current` as the thing you poll against.

```ts
it('should load async data', async () => {
    const { waitFor, result } = renderHook(() => useDocument());

    // initially data is in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for the data to be ready
    await waitFor(() => result.current.state === 'READY');

    // verify the documentData exists
    expect(result.current.cimDoc).toMatchObject({ /* ... * / })
});
```

## When not to use this library?

Does this mean you should use this package whenever you write a hook? No.

This library does not write anything to JS DOM in a way that is accessible via the `screen` API. So if you have a hook that affects UI/DOM state (e.g. sets focus on an element), you'll still want to use the standard RTL library.

Example`useSetFocus.test.tsx`

```tsx
const useSetFocus = <T extends HTMLElement>(options: Params<T>) => {
    const { applyFocus, id, customRef } = options;
    const inputRef = useRef<T | null>(null);
    const ref = customRef ?? inputRef;

    useLayoutEffect(() => {
        if (applyFocus && ref.current) {
            ref.current.focus();

            // put cursor at the end if the element is a <textarea />
            if (ref.current instanceof HTMLTextAreaElement) {
                const endIndex = ref.current.value.length;
                ref.current.setSelectionRange(endIndex, endIndex);
            }
        }
    }, [applyFocus, id, ref]);

    return ref;
};

it('should leave cursor at the beginning of an <input />', () => {
    const testValue = 'Hello world';
    const TextAreaWrapper: React.FC<{ children: string }> = props => {
        const ref = useSetFocus<HTMLInputElement>({ applyFocus: true });

        return <input ref={ref} data-testid="test" defaultValue={props.children} />;
    };

    render(<TextAreaWrapper>{testValue}</TextAreaWrapper>);

    expect((document.activeElement as HTMLTextAreaElement).selectionEnd).toEqual(0);
});

it('should place the cursor at the end of a <textarea />', () => {
    const testValue = 'Hello world';
    const TextAreaWrapper: React.FC<{ children: string }> = props => {
        const ref = useSetFocus<HTMLTextAreaElement>({ applyFocus: true });

        return <textarea ref={ref} data-testid="test" defaultValue={props.children} />;
    };

    render(<TextAreaWrapper>{testValue}</TextAreaWrapper>);

    expect((document.activeElement as HTMLTextAreaElement).selectionEnd).toEqual(testValue.length);
});
```

### Exercise

Rewrite the `DocumentProvider` tests using the hooks library `07-react-testing-library-hooks/exercise/__tests__/useDocument.test.ts`
