import React, { createContext, useState } from 'react';

export const ToggleSelectionContext = createContext<string | undefined>(undefined);
export const ToggleSetSelectionContext = createContext<((value: string) => void) | undefined>(undefined);

export const ToggleProvider: React.FC<{ defaultSelectionId?: string }> = props => {
    const [selectedId, setSelectedId] = useState(props.defaultSelectionId);

    return (
        <ToggleSelectionContext.Provider value={selectedId}>
            <ToggleSetSelectionContext.Provider value={setSelectedId}>
                {props.children}
            </ToggleSetSelectionContext.Provider>
        </ToggleSelectionContext.Provider>
    );
};
