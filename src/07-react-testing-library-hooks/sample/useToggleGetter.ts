import { useContext, MouseEventHandler, ButtonHTMLAttributes } from 'react';
import { ToggleSelectionContext, ToggleSetSelectionContext } from './ToggleProvider';

type AriaPressed = { 'aria-pressed': boolean };

type PropGetterReturn<T> = Omit<T, 'onClick'> & {
    onClick: MouseEventHandler<HTMLButtonElement>;
} & AriaPressed;

// use normal Button HTML attributes and then extend Record<string, string> to support
// arbitrary data-* attributes
type ExtendedHTMLAttributes = ButtonHTMLAttributes<HTMLButtonElement> & Record<string, any>;

export const useToggleGetter = <T extends ExtendedHTMLAttributes>(value: string) => {
    const selection = useContext(ToggleSelectionContext);
    const setSelection = useContext(ToggleSetSelectionContext);

    if (selection === undefined || setSelection === undefined) {
        throw new Error('Component must be wrapped in a ToggleProvider');
    }

    return (props?: T): PropGetterReturn<T> => {
        const { onClick, ...rest } = (props || {}) as T;
        return {
            ...rest,
            onClick(event) {
                event.persist();
                setSelection(value);

                if (onClick) {
                    onClick(event);
                }
            },
            'aria-pressed': value === selection,
        };
    };
};
