import { renderHook, act } from '@testing-library/react-hooks';
import { useToggleGetter } from '../useToggleGetter';
import { ToggleProvider } from '../ToggleProvider';
import React from 'react';

it('should throw an error when not wrapped in a <ToggleProvider />', () => {
    const { result, rerender, unmount, waitForNextUpdate, waitForValueToChange } = renderHook(() =>
        useToggleGetter('doesnt matter')
    );

    expect(result.error).toMatchInlineSnapshot(`[Error: Component must be wrapped in a ToggleProvider]`);
});

it('should return back the props I provide it', () => {
    const { result } = renderHook(() => useToggleGetter('doesnt matter'), {
        wrapper: ToggleProvider,
        initialProps: { defaultSelectionId: 'abc' },
    });

    expect(result.current({ 'data-foo': 'foo', className: 'hello-world' })).toMatchObject({
        'data-foo': 'foo',
        className: 'hello-world',
    });
});

it('should support passing it no props', () => {
    const { result } = renderHook(() => useToggleGetter('doesnt matter'), {
        wrapper: ToggleProvider,
        initialProps: { defaultSelectionId: 'abc' },
    });

    expect(result.current()).toBeDefined();
    expect(result.error).toBeUndefined();
});

it('should set the aria-pressed state based on defaultValue of ToggleProvider', () => {
    const { result: selectedResult } = renderHook(() => useToggleGetter('abc'), {
        wrapper: ToggleProvider,
        initialProps: { defaultSelectionId: 'abc' },
    });

    // verify aria-pressed is true since useToggleGetter() value and defaultSelectionId match
    expect(selectedResult.current()).toMatchObject({
        'aria-pressed': true,
    });
});

it('should update the aria-pressed state when changing selection', () => {
    const { result } = renderHook(() => useToggleGetter('def'), {
        wrapper: ToggleProvider,
        initialProps: { defaultSelectionId: 'abc' },
    });

    // verify the value is not selected at first ('def' !== 'abc')
    expect(result.current()).toMatchObject({
        'aria-pressed': false,
    });

    const mockEvent = { persist() {} } as React.MouseEvent<HTMLButtonElement>;

    act(() => {
        result.current().onClick(mockEvent);
    });

    // verify the value is now selected
    expect(result.current()).toMatchObject({
        'aria-pressed': true,
    });
});

it('should call my onClick event', () => {
    const { result } = renderHook(() => useToggleGetter('doesnt matter'), {
        wrapper: ToggleProvider,
        initialProps: { defaultSelectionId: 'also doesnt matter' },
    });

    const mockOnClick = jest.fn() as React.MouseEventHandler<HTMLElement>;

    const props = result.current({ onClick: mockOnClick });
    const mockEvent = { persist() {} } as React.MouseEvent<HTMLButtonElement>;

    act(() => {
        props.onClick(mockEvent);
    });

    // verify custom onClick was called with the event object
    expect(mockOnClick).toBeCalledWith(mockEvent);

    // verify the value is now selected
    expect(result.current()).toMatchObject({
        'aria-pressed': true,
    });
});
