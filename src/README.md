# Hello. Welcome. 👋 and 🙏

## Why a React Testing Library + Jest Workshop

### Powerful but awkward API

-   React Testing Library (RTL) has really great dev experience _if_ you learn the relatively large API of the library. Otherwise, it is a bit confusing (e.g. `findBy*` vs. `queryBy*` vs. `getBy*`)
-   If you learn the library, I think some of the awkward API decisions the author(s) made start to feel reasonable and can be leveraged to make really robust tests

### Author tests the way a user *uses* the component

I appreciate and largely agree with the perspective of RTL's author, Kent C. Dodds, regarding how you should author tests: **test as a user would and in an accessible way**. This resonates with me as someone that has used Enzyme in the past. While I found testing to be generally easier with Enzyme, I was often testing private implementation details and not testing the the output - what a user of the component actualy sees (with their eyes or via assistive technology).

For this reason, RTL *discourages* selecting or interacting with elements based on attributes that are "invisible" to users such as a `className` or `id`.

### Jest is the backbone of everything

To be effective using RTL, you need a good foundation in Jest. This requires understanding things like test structure, mocks, spies, assertions, and other Jest "magic".

### These tools are not perfect

RTL + Jest are far from perfect. There are certain behaviors that are not possible to test. This is in part a limitation of JS DOM and also some of the opinions of testing library making some test difficult or impossible. I want to talk about those cases

## Why not Cypress?

Cypress does not lend itself well to component testing. Cypress requires you visit a URL in the browser, thus it is better suited to full page/app testing. For this reason, I tend to lean on Cypress for high level, *happy path* testing. However, RTL, Jest, and related tools compliment Cypress and are great for going a level deeper to tes\- more complex issues and edge cases.

Of note, Cypress has a feature called **Cypress Component Testing** in alpha that looks promising. https://docs.cypress.io/guides/component-testing/introduction.

## Using this course material

In the interest of time, I may need to skip over some sections. Thankfully, it is all documented here for you to read and refer back to.

**Do the exercices.** These activities will arrest forgetting and surface gaps in your own knowledge. 

I also welcome questions directly. Feel free to slack me @Eric or email me emasiello@vistaprint.com.
