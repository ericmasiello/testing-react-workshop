import { useEffect } from 'react';
import { useThrowAsyncErrorToBoundary } from './useThrowAsyncErrorToBoundary';
type UseEffectParams = Parameters<typeof useEffect>;
type UseEffectCallbackReturn = ReturnType<Parameters<typeof useEffect>['0']>;

type CallbackParam = {
    throwErrorToBoundary: ReturnType<typeof useThrowAsyncErrorToBoundary>;
    abortSignal: AbortSignal | undefined;
    runIfMounted: (fn: Function) => void;
    isComponentMounted: boolean;
};

type UseAbortbableFetch = (fn: (param: CallbackParam) => UseEffectCallbackReturn, deps?: UseEffectParams['1']) => void;

export const useAsyncEffect: UseAbortbableFetch = (fn, deps = []) => {
    const throwErrorToBoundary = useThrowAsyncErrorToBoundary();
    useEffect(() => {
        let isComponentMounted = true;
        let abortController: AbortController | null = null;

        if (window.AbortController) {
            abortController = new AbortController();
        }

        const runIfMounted: CallbackParam['runIfMounted'] = callback => {
            if (!isComponentMounted) {
                return;
            }
            callback();
        };

        const runIfMountedThrowErrorToBoundary = (e: any) => {
            runIfMounted(() => throwErrorToBoundary(e));
        };

        const cleanup = fn({
            abortSignal: abortController?.signal,
            runIfMounted,
            isComponentMounted,
            throwErrorToBoundary: runIfMountedThrowErrorToBoundary,
        });

        return () => {
            abortController?.abort();
            isComponentMounted = false;
            if (cleanup) {
                cleanup();
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [...deps, throwErrorToBoundary]);
};
