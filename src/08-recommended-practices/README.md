# Recommended Practices

## Prefer integration tests and minimize mocking to only network requests (when possible).

Avoid mocking providers or other integration points. Too much mocking limits the utility of your tests and does not provide as much confidence in what you are writing.

Let's use our previous `useDocument()` hook which depended on a `DocumentProvider` which, in turn, makes a network request. We _could_ have mocked any number of things to test the case where `useDocument()` gives you back a valid result.

```ts
export const useDocument = () => {
    const context = useContext(DocumentContext);
    if (context === undefined) {
        throw new Error('Your component must be wrapped in a DocumentProvider');
    }
    return context;
};
```

```ts
// ❌ Mock `useContext` to return a specific value
jest.mock('react', () => {
    const react = jest.requireActual('react');
    return {
        ...react,
        useContext() {
            return {
                state: 'READY',
                cimDoc: { foo: 'bar' },
            };
        },
    };
});

it('should fetch the document after loading', () => {
    const { result, waitFor } = renderHook(() => useDocument());

    expect(result.current.state).toBe('READY');

    // assert that cimDoc is no longer null
    expect(result.current.cimDoc).not.toBeNull();
    // assert there are no errors
    expect(result.current.errorMessage).toBeUndefined();
});
```

But what did you actually test here? Very little. We really only tested that not returning `undefined` will not `throw`. But more importantly, we missed all the asynchronous behavior of how this hook _actually_ works.

Instead, _integrate_ all the parts (i.e. `DocumentProvider`) and test it how the component will be used: _asynchronously_.

```ts
// The only thing mocked here is the HTTP response via MSW

it('should fetch the document after loading', async () => {
    const { result, waitFor } = renderHook(() => useDocument(), {
        wrapper: DocumentProvider,
        initialProps: { docId: 'abc' },
    });

    // initially in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for data to be ready
    await waitFor(() => result.current.state === 'READY');

    // assert that cimDoc is no longer null
    expect(result.current.cimDoc).not.toBeNull();
    // assert there are no errors
    expect(result.current.errorMessage).toBeUndefined();
});
```

Also, technically the above test does not need this:

```typescript
// initially in a loading state
expect(result.current.state).toBe('LOADING');
```

However, I like to include these intermediate states in my tests as I feel they better express the "story" of how this component/hook operates.

1. It is in a loading state.
2. Once it fetches the data, it is in a ready state with the data and no error message

## Don't DRY up your code too much. Treat tests like documentation.

It can be helpful to write helper functions or components that wrap up logic you're performing over and over in different tests. But don't be afraid to have your tests be a little wet (repeated/copy & pasted code).

```typescript
import { render, screen } from '@testing-library/react';
import App from '../App';

const renderApp = (testOptions) => {
  // < pretend a bunch of setup stuff is here based on testOptions >
  render(<App {...aBunchOfHardcodedProps} />);
}

it('should ...', () => {
  renderApp('renderThisWay');

  expect(...).toBe(...);
})

it('should also...', () => {
  renderApp('renderThatWay');

  expect(...).toBe(...);
})

it('should this other thing...', () => {
	renderApp('renderThisOtherWay');

  expect(...).toBe(...);
})
```

Personally, I'd rather as much (as reasonable) of the contents of `renderApp()` copy/pasted between each `it` block. Abstracting all the "arrange" and "act" parts of your tests away from the place you're making the assertion makes it difficult to read the tests from a documentation standpoint. If I'm trying to understand the behavior of a component or function and I need to keep scrolling up and down to see how the data is being passed in to yield some particular behavior, I'm personally going to have a difficult time parsing these tests. (https://automationpanda.com/2020/07/07/arrange-act-assert-a-pattern-for-writing-good-tests/)

### However, abstractions can be useful so long as they add _clarity_ - not reduce it.

As an example, if you are testing a component and that component requires it be nested in several of providers, all of which require several props passed to them, writing all that into the `it()` block can actually diminish clarity. In these cases, it becomes *less* clear which pieces of data are making the component act _this_ way in *this* test and _that_ way in another test.

Test abstractions should be used to **improve the signal/noise ratio**.

```tsx
// ContextualPopover.test.tsx
// ❌ too much noise. I can't tell what is different between each test block

it('should act this way', () => {
  render(
		<TrackingProvider
      requestor="requestor"
      docUri="http://www.whatever.com"
      locale="en-ie"
      productAttributes={{...}}
      productKey="PRD-12345"
      productVersion={1}
    >
      <DesignRequirementsLoaderProvider requestor="requestor" {...designRequirementProps}>
        <DesignEngineProvider
          accessToken="the token"
          requestor="requestor"
          initialMinimumViewMargins={...}
        >
          <Loader>Loading...</Loader>
		      <ContextualPopover data-testid="popover" value="hello world" />
        </DesignEngineProvider>
      </DesignRequirementsLoaderProvider>
    </TrackingProvider>
  )

  expect(...).toBe(...);
})

it('should also act this other way', () => {
  render(
		<TrackingProvider
      requestor="requestor"
      docUri="http://www.whatever.com"
      locale="en-ie"
      productAttributes={{...}}
      productKey="PRD-12345"
      productVersion={1}
    >
      <DesignRequirementsLoaderProvider requestor="requestor" {...designRequirementProps}>
        <DesignEngineProvider
          accessToken={null}
          requestor="requestor"
          initialMinimumViewMargins={...}
        >
          <Loader>Loading...</Loader>
		      <ContextualPopover data-testid="popover" value="hello world" />
        </DesignEngineProvider>
      </DesignRequirementsLoaderProvider>
    </TrackingProvider>
  )

  expect(...).toBe(...);
})
```

Instead, abstract away the providers into a shared component and only pass the relevant changes to the abstraction.

```tsx
// ContextualPopover.test.tsx
// ✅ Creating a simple test provider makes it easy to spot the differences between each test run

it('should act this way', () => {
  render(
    // <TestProviders /> wraps all those other providers and sets reasonable defaults
		<TestProviders>
      <Loader>Loading...</Loader>
      <ContextualPopover data-testid="popover" value="hello world" />
    </TestProviders>
  )

  expect(...).toBe(...);
})

it('should also act this other way', () => {
  render(
    // passing the authToken explicitly will override the default value
    // causing ContextualPopover to act differently
		<TestProviders authToken={null}>
      <Loader>Loading...</Loader>
      <ContextualPopover data-testid="popover" value="hello world" />
    </TestProviders>
  )

  expect(...).toBe(...);
})
```

Now you might then wonder: *what's the difference here with `TestProviders` versus the `renderApp()` function in the previous example?*

### The *subject* of the test should always be visible in each `it()` block

In the `TestProviders` example, the the thing we were actually focusing our tests on was `ContextualPopover`. You'll note that it was rendered in each `it()` whereas in the `renderApp()` example, the test subject (`App`) was part of the abstraction.

### Abstractions should not hide implementation details that are relevant to the behavior of the test

When you do write helper functions/components, be sure to pass in values to those helpers that make it clear in each test how those helpers are being used:

```tsx
// DocumentProvider.test.tsx

const Loader: React.FC = (props) => {
  const { state, cimDoc } = useDocument();
  if (state !== 'READY') {
    return <div>Loading...</div>
  }
  return <>{props.children}</>
}

it('should render the document' async () => {
  render(
    <DocumentProvider>
    	<Loader>
        <div>I am loaded!</div>
      </Loader>
    </DocumentProvider>
  );

  // ❌ not clear in the it() block where the "Loading..." text you're waiting on came from
  await screen.findByByText('Loading...'');

  expect(await screen.findByText('I am loaded')).toBeDefined();
});
```

```tsx
const Loader: React.FC<{ loadingMessage: string }> = (props) => {
  const { state, cimDoc } = useDocument();
  if (state !== 'READY') {
    return <div>{props.loadingMessage}</div>
  }
  return <>{props.children}</>
}

it('should render the document' async () => {
  render(
    <DocumentProvider>
      <Loader loadingMessage="Loading...">
        <div>I am loaded!</div>
      </Loader>
    </DocumentProvider>
  );

  // now its clearer just by reading this it() block where "Loading..." is coming from
  await screen.findByByText("Loading...");

  expect(await screen.findByText("I am loaded")).toBeDefined();
});
```

### Render props can be a useful tool in tests

If your test requires you work with data provided by a hook and you wish to change what that data looks like during each test case, render props can be useful technique in using simple abstractions that still provide test clarity.

_Note: render props isn't a particularly popular pattern anymore so don't expect everyone to be familiar with it. If you use it, heavily comment the render prop component to explain how it works._

```tsx
// ItemSelection.test.tsx

/**
 * ActivePanelItems is a component that accepts a children prop as a function. The children function is then
 * passed all the items available on the activePanel. The consumer of ActivePanelItems can then take those values,
 * map over them, and render whatever they want with each item.
 **/
type ItemsToReactElement = (items: DesignItemType[]) => React.ReactElement;
type Props = { children: ItemsToReactElement };

const ActivePanelItems = observer((props: Props) => {
    const { children } = props;
    const designEngine = useDesignEngineReady();

    useEffect(() => {
        designEngine.layoutManager.setActiveDesignPanelId(designEngine.panels[0].id);
        designEngine.idaManager.setVisibleDesignPanelIds([designEngine.panels[0].id]);
    }, [designEngine]);

    return children(designEngine.layoutManager.activePanel?.items || []);
});

it('should render a button for each item', async () => {
    const cimDoc = createTestCimDoc({
        // add 3 items to our active (first) panel
        0: builder =>
            builder
                .addTextAreaItem()
                .addLineItem()
                .addItemReference(),
    });

    server.use(
        rest.get('https://storage.documents.cimpress.io/v3/documents/*', (_, res, ctx) => {
            return res(ctx.status(200), ctx.json(cimDoc));
        })
    );

    render(
        <FocusManagerProvider>
            <DesignExperienceTestProvider>
                <ActivePanelItems>
                    {items => (
                        <>
                            {items.map(item => (
                                <ItemSelection data-testid="item-selection" key={item.id} item={item} />
                            ))}
                        </>
                    )}
                </ActivePanelItems>
            </DesignExperienceTestProvider>
        </FocusManagerProvider>
    );

    // Then, make assertions about how a "text area" vs. "line item" vs "item reference" render as
    // a <ItemSelection />
});
```

### Your integration tests should tell a story

It's a pretty boring story but a story nevertheless.

It may sound painfully obvious but liberal use of small comments, particularly before each `await` and `expect` can go a long way in explaining how the component is intended to behave.

```tsx
// useDocument.ts

it('should fetch the document after loading', async () => {
    const { result, waitFor } = renderHook(() => useDocument(), {
        wrapper: DocumentProvider,
        initialProps: { docId: 'abc' },
    });

    // initially in a loading state
    expect(result.current.state).toBe('LOADING');

    // wait for data to be ready
    await waitFor(() => result.current.state === 'READY');

    // assert that cimDoc is no longer null
    expect(result.current.cimDoc).not.toBeNull();
    // assert there are no errors
    expect(result.current.errorMessage).toBeUndefined();
});
```

## "Code Coverage Driven Development"

How much test coverage is appropriate? Ultimately, I think that answer depends on a few factors. Lower level components such as those you'd find in a library should cover most/all edge cases. Application level flows likely require you to focus on key parts of your app experience since the surface area is larger.

When I sit down to write tests for a component, the first few tests I need to write are often obvious. Test the happy path. If I expect a hook to fetch some data, write a test to make sure it does that. But thanks to the syntax of JavaScript, there are tons of conditions tucked behind syntactical sugar.![Code Coverage Screenshot](codecoverage.png)

Take for example line `44` which says `abortController?.abort()`. That line will only run the `abort()` method _if_ `abortController` has the method defined.

To answer what I should test next, I use the `--coverage` flag when running `jest` and then inspect the report. Anything that looks like a critical piece of functionality that I want to make sure works as expected, I'll find ways to introduce additional tests.
