## Asynchronous Testing with React Testing Library

Much of the code we write depends on asynchronous behavior. Often, that requires fetching data from a remote API, putting that data into state, and then rendering a UI that reflects that data. In the interim, we may render a loading indicator. These are all behaviors we should test as part of an RTL based integration test suite (using MSW to mock the responses).

Take the following example component:

```tsx
export const App = () => {
    const [pokemonName, setPokemonName] = useState('');
    const [pokemonAbilities, setPokemonAbilities] = useState<string[] | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        let isMounted = true;
        const execute = async () => {
            setIsLoading(true);
            const abilities = await fetchPokemonAbilities(pokemonName);
            if (isMounted) {
                setPokemonAbilities(abilities);
                setIsLoading(false);
            }
        };
        if (pokemonName) {
            execute();
        }
        return () => {
            isMounted = false;
        };
    }, [pokemonName]);

    if (isLoading) {
        return <Loader />;
    } else if (pokemonAbilities?.length) {
        return (
            <PokemonAbilities
                name={pokemonName}
                abilities={pokemonAbilities}
                onReset={() => {
                    setPokemonName('');
                    setPokemonAbilities(null);
                }}
            />
        );
    } else {
        return <PokemonForm setPokemonName={setPokemonName} />;
    }
};
```

The UI will present 3 different states based on the internal values of the multiple `useState()` hooks.

1. Which UI state is the initial state you would expect to see when first loading the form?
2. How do we trigger the next state?
3. And how do we trigger the final state?
4. Can each of these state changes be observed synchronously (i.e. _immediately_) as a result of a user interaction? If not, which ones cannot?

The initial state renders the form. Presumably, the user interacts with the form which sets the Pokemon name. This kicks off the `useEffect` which, in turn, immediately renders the loading spinner. At that point, we are now in an `await` state as we fetch the data from the Pokemon API. We should continue to render the loading spinner until we successfully get the data back. At which point, the abilities are set, the loader is set to false, and we should render the `PokemonAbilities` component.

But how do we handle that waiting state? Even if we did mock the `fetchPokemonAbilities` to resolve with some specified data, it still needs to behave as a promise which means it won't give us a response immediately.

## Handling Async with RTL

First, let's verify the form is rendered successfully. This can be handled synchronously via `getBy*`

```tsx
it('should initially render a form to input the pokemon name', () => {
    render(<App />);

    // use the <input /> as a proxy to know we have rendered the form
    const input = screen.getByRole('textbox');

    expect(input).toBeDefined();
});
```

Then, let's submit our form and verify we can see the loading spinner (still synchronous `getBy*`)

```tsx
it('should render a loading spinner after submitting the form', () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    userEvent.type(pokemonField, 'pikachu{enter}');

    // find and verify the loading indicator is present via its role
    const loader = screen.getByRole('status');
    expect(loader).toBeDefined();
});
```

Now let's assert that that we see the data.

```tsx
it('should render the results after the data is fetched (using waitFor())', async () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    await userEvent.type(pokemonField, 'pikachu{enter}');

    // verify the loading indicator is no present
    screen.getByRole('status');

    // use the `waitFor` command from RTL
    await waitFor(() => {
        const title = screen.getByText(/pikachu abilities/i);
        const firstAbility = screen.getByText(/static/i);
        const secondAbility = screen.getByText(/lightning-rod/i);

        expect(title).toBeDefined();
        expect(firstAbility).toBeDefined();
        expect(secondAbility).toBeDefined();
    });
});
```

### `waitFor()`

`waitFor` is an async operation that continually executes its callback function until it either times out (in which case the tests fail) *or* the entire body of the function executes without throwing an error.

In the above example, any of the 6 lines of code within the `waitFor` callback _could_ throw an error.

-   `screen.getBy*` immediately throw an error if the value is not present
-   an `expect()` assertion will throw an error if the assertion is not true

Initially, none of those things within the `waitFor()` are true so presumably even the first line of code (`const title = screen.getByText(/pikachu abilities/i);`) is throwing an error. _But_, after time, the data should resolve and the UI should be present. This will make the tests pass.

### `findBy*` queries

The idea of wrapping `getBy*` within an `waitFor()` is such a common pattern that RTL exposes an async compliment to `getBy` called `findBy`. This is also  asynchronous and behaves exactly the same as `waitFor()` in that it continually polls _until_ the expression is true or the the `findBy*` times out. The above test can be rewritten as:

```tsx
it('should render the results after the data is fetched (using findBy*())', async () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    await userEvent.type(pokemonField, 'pikachu{enter}');

    // verify the loading indicator is no present
    screen.getByRole('status');

    // verify all the resultin data is available
    const title = await screen.findByText(/pikachu abilities/i);
  	// QUIZ: why is getBy* okay here??
    const firstAbility = screen.getByText(/static/i);
    const secondAbility = screen.getByText(/lightning-rod/i);

    expect(title).toBeDefined();
    expect(firstAbility).toBeDefined();
    expect(secondAbility).toBeDefined();
});
```

**Question**: why did I only use `findByText` on the title and not the abilities?

### So when should I use `waitFor()`?

There are two types of cases where I've found myself reaching for a `waitFor()` over a/series of `findBy*`s.

#### Looping over data

There's no way to loop over data using a `findBy*` construct. So if you're dealing with a collection of data that will _eventually_ hold some state and you need to make assertions on each item in that array, use an `waitFor()`

```typescript
await waitFor(() => {
    /*
     * In this scenario, designViews is some data that will *eventually* have values set. `waitFor` continually
     * executes this code until all `expect()` assertions are true
     */
    designViews.forEach(view => {
        const matchedPanel = expectedProperties.find(panel => panel.id === view.id);

        return expect(view).toMatchObject(matchedPanel); // this will throw an error if assertion fails
    });
});
```

#### Async assertions of non-DOM related data

The other case is when you want to make an async / eventual assertion against something that isn't DOM related.

```typescript
/*
const addToCartHandler = async () => {
	const result = await someApiCall();
	
	if (onProductAddedToCart) {
		onProductAddedToCart(
			result.workId,
			result.createdDocumentUrl,
			price.quantity
		);
	}
}

return (
  <button
    onClick={() => {
      addToCartHandler();
    }}
  >
    Add to Cart
  </button>
);
*/


const addToCartButton = await screen.findByTestId("add-to-cart-button");
fireEvent.click(addToCartButton);

await waitFor(() => {
    // "click" happens in an async function so we need to wrap this in waitFor
    expect(onProductAddedToCart).toHaveBeenCalledWith('workId', 'documentUrl', 1);
});
```

Its generally recommended that you don’t put a bunch of of assertions/`getBy*` within a `waitFor`. It is better to assert things one at a time using a `findBy*` (which is really just a `getBy*` wrapped in a `waitFor()`). However, in this case, because we have a loop we want to run assertions on, this is an okay practice.

The downside to this approach is the error message you’ll receive in the console will be much less clear than if you had individually applied `findBy*` to each element.

#### Async asserting that something does not exist

Remember, the `queryBy*` style queries are the only way you can assert that something is `null` (ie does not exist) without the query immediately throwing an error. However, there is no async form of `queryBy*`. To solve this, wrap your `queryBy` in a `waitFor`.

```typescript
// wait for loader to appear...
await screen.findByTestId('DesignExperience-Loader');

// now verify the loader is gone
await waitFor(() => {
    expect(screen.queryByTestId('DesignExperience-Loader')).toBeNull();
});
```

### Exercise

Implement `06-react-testing-library-async/exercise/__tests__/DocumentProvider.test.tsx`
