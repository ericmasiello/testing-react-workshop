import React, { createContext, useReducer, useMemo, useContext } from 'react';
import { useAsyncEffect } from '../../libs/easel-async-hooks-react';
import { fetchDocument } from './fetchDocument';
import { CimDoc } from './types/CimDoc';

export enum DocumentAction {
    CIMDOC_LOADED = 'CIMDOC_LOADED',
    LOADING_CIMDOC = 'LOADING_CIMDOC',
    LOADING_ERROR = 'LOADING_ERROR',
}

export enum DocumentState {
    UNINITIALIZED = 'UNINITIALIZED',
    LOADING = 'LOADING',
    READY = 'READY',
    ERROR = 'ERROR',
}

export type DocumentProviderState = {
    state: DocumentState;
    cimDoc: CimDoc | null;
    docId?: string;
    errorMessage?: string;
};

export type CimDocLoadSuccess = {
    type: DocumentAction.CIMDOC_LOADED;
    props: {
        cimDoc: CimDoc;
        docId?: string;
    };
};

type LoadingCimDoc = {
    type: DocumentAction.LOADING_CIMDOC;
    props: string;
};

type ErrorLoadingCimDoc = {
    type: DocumentAction.LOADING_ERROR;
    props?: string;
};

type CommandAction = CimDocLoadSuccess | LoadingCimDoc | ErrorLoadingCimDoc;

const reducer = (state: DocumentProviderState, action: CommandAction) => {
    switch (action.type) {
        case DocumentAction.LOADING_CIMDOC: {
            return {
                ...state,
                state: DocumentState.LOADING,
                cimDoc: null,
                docId: action.props,
            };
        }
        case DocumentAction.CIMDOC_LOADED: {
            const { cimDoc, docId } = action.props;

            return {
                ...state,
                state: DocumentState.READY,
                cimDoc,
                docId: docId ?? undefined,
            };
        }
        case DocumentAction.LOADING_ERROR: {
            return {
                ...state,
                state: DocumentState.ERROR,
                cimDoc: null,
                errorMessage: action.props,
            };
        }
        default: {
            return state;
        }
    }
};

type DocumentContextState =
    | {
          state: DocumentState.READY;
          cimDoc: CimDoc;
          errorMessage: undefined;
      }
    | {
          state: DocumentState.LOADING;
          cimDoc: null;
          errorMessage: undefined;
      }
    | {
          state: DocumentState.UNINITIALIZED;
          cimDoc: null;
          errorMessage: undefined;
      }
    | {
          state: DocumentState.ERROR;
          cimDoc: null;
          errorMessage?: string | undefined;
      };

export const DocumentContext = createContext<DocumentContextState | undefined>(undefined);

export type DocumentProviderProps = {
    /**
     * When passed, the `DocumentProvider` will fetch the document associated with this URI and provide it to children. This prop will override `cimDoc`
     */
    docId?: string;
    /**
     * When passed, the `DocumentProvider` will provide this CimDoc without refetching from UDS. This data is overridden by the `docId` prop.
     */
    cimDoc?: CimDoc | null;
};

export const DocumentProvider: React.FC<DocumentProviderProps> = props => {
    const [contextValue, dispatch] = useReducer(reducer, {
        cimDoc: props.cimDoc ?? null,
        docId: props.docId,
        state: props.cimDoc ? DocumentState.READY : DocumentState.UNINITIALIZED,
    });

    // load the cimDoc
    useAsyncEffect(
        ({ abortSignal, runIfMounted }) => {
            const execute = async (docId: string) => {
                try {
                    dispatch({
                        type: DocumentAction.LOADING_CIMDOC,
                        props: docId,
                    });

                    const userDocument = await fetchDocument(docId, abortSignal);

                    runIfMounted(() => {
                        dispatch({
                            type: DocumentAction.CIMDOC_LOADED,
                            props: {
                                cimDoc: userDocument,
                                docId: docId,
                            },
                        });
                    });
                } catch (error) {
                    runIfMounted(() => {
                        dispatch({
                            type: DocumentAction.LOADING_ERROR,
                            props: error instanceof Error ? error.message : undefined,
                        });
                    });
                    console.error(error);
                }
            };

            if (props.docId) {
                execute(props.docId);
            }
        },
        [props.docId]
    );

    const value: DocumentContextState = useMemo(() => {
        switch (contextValue.state) {
            case DocumentState.ERROR:
                return {
                    state: DocumentState.ERROR,
                    cimDoc: null,
                    errorMessage: contextValue.errorMessage,
                };
            case DocumentState.UNINITIALIZED:
                return {
                    state: DocumentState.UNINITIALIZED,
                    cimDoc: null,
                    errorMessage: undefined,
                };
            case DocumentState.LOADING:
                return {
                    state: DocumentState.LOADING,
                    cimDoc: null,
                    errorMessage: undefined,
                };
            default:
                return {
                    state: DocumentState.READY,
                    cimDoc: contextValue.cimDoc!,
                    errorMessage: undefined,
                };
        }
    }, [contextValue.cimDoc, contextValue.state, contextValue.errorMessage]);

    return <DocumentContext.Provider value={value}>{props.children}</DocumentContext.Provider>;
};

export const useDocument = () => {
    const context = useContext(DocumentContext);
    if (context === undefined) {
        throw new Error('Your component must be wrapped in a DocumentProvider');
    }
    return context;
};
