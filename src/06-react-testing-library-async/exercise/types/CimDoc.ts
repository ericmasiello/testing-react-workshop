export interface Coordinates {
    x: string;
    y: string;
}
export interface Dimensions {
    width: string;
    height: string;
}
export declare type Position = Dimensions & Coordinates;
export interface ImageOverlay {
    color: string;
    printUrl: string;
    previewUrl: string;
}
export interface ColorOverrides {
    ordinal: number;
    color: string;
}
export interface CropFractions {
    bottom: string;
    top: string;
    left: string;
    right: string;
}
export interface ColorAdjustment {
    hueMultiplier: number;
    /**
     * hueOffset is in degrees [0, 360)
     */
    hueOffset: number;
    /**
     * Multipliers can be any number, but are generally in the range [0, 2].
     */
    saturationMultiplier: number;
    /**
     * Saturation and lightness offsets can be any number, but are generally in the range [0, 2].
     */
    saturationOffset: number;
    lightnessMultiplier: number;
    lightnessOffset: number;
}
export interface ImageItem {
    id: string;
    zIndex: number;
    position: Position;
    rotationAngle?: string;
    horizontalAlignment?: string;
    verticalAlignment?: string;
    printUrl?: string;
    previewUrl: string;
    originalSourceUrl?: string;
    pageNumber: number;
    overlays?: ImageOverlay[];
    cropFractions?: CropFractions;
    colorOverrides?: ColorOverrides[];
    vpThreadMapping?: string;
    colorAdjustment?: ColorAdjustment;
}
export interface TextAttributes {
    fontSize: string;
    fontStyle: string;
    fontFamily: string;
    color: string;
    overprints?: string[];
    inlineBaseDirection?: string;
}
export interface TextField extends TextAttributes {
    type?: 'inline';
    content: string;
}
export interface TextInline extends Partial<TextAttributes> {
    type?: 'inline';
    content: string;
}
export declare type ListStyle = 'ordered' | 'unordered';
export interface TextListItem extends Partial<TextAttributes> {
    type: 'listItem';
    listMarkerStyle: ListStyle;
    depth?: number;
    content: TextInline[];
}
export interface TextList extends TextAttributes {
    type: 'list';
    content: TextListItem[];
}
export declare type AnyTextContent = TextField | TextList | TextListItem | TextInline;
export declare type TextAreaContent = TextField | TextList;
export interface TextAreaItem {
    id: string;
    zIndex: number;
    position: Position;
    rotationAngle?: string;
    horizontalAlignment?: string;
    verticalAlignment?: string;
    blockFlowDirection?: string;
    textOrientation?: string;
    content: TextAreaContent[];
}
export interface ItemReference<T = any> {
    id: string;
    type?: string;
    url: string;
    specName?: string;
    data: T;
    zIndex: number;
    rotationAngle?: string;
    position: Position;
}
export interface Stroke {
    color: string;
    thickness: string;
    lineCap?: string;
    lineJoin?: string;
}
export interface StrokeLineAttributes {
    lineCap: string;
    lineJoin: string;
}
export declare enum Operation {
    MoveTo = 'moveTo',
    LineTo = 'lineTo',
    Close = 'close',
}
export interface Curve {
    operation: Operation;
    position: Coordinates;
    controlPoint1?: Coordinates;
    controlPoint2?: Coordinates;
}
export interface LineItem {
    readonly type: 'line';
    id: string;
    zIndex: number;
    rotationAngle?: string;
    start: Coordinates;
    end: Coordinates;
    stroke: Stroke & StrokeLineAttributes;
    overprints?: string[];
}
export interface CurveItem {
    readonly type: 'curve';
    id: string;
    position: Position;
    zIndex: number;
    rotationAngle?: string;
    stroke?: Stroke;
    color: string;
    curves: Curve[];
    viewBox: Position;
    closeBehavior: number;
    overprints?: string[];
}
export interface FigureItem {
    readonly type: 'rectangle' | 'ellipse';
    id: string;
    position: Position;
    zIndex: number;
    rotationAngle?: string;
    stroke?: Stroke;
    color: string;
    overprints?: string[];
}
export interface ComparisonImage {
    key: string;
    url: string;
}
export declare type ShapeItem = LineItem | FigureItem | CurveItem;
export declare type Item = ImageItem | TextAreaItem | ShapeItem | ItemReference;
export interface Ornament {
    type: string;
    positions: Coordinates;
}
export interface Panel {
    id: string;
    name: string;
    width: string;
    height: string;
    decorationTechnology: string;
    images?: ImageItem[];
    textAreas?: TextAreaItem[];
    itemReferences?: ItemReference[];
    shapes?: ShapeItem[];
    comparisonImages?: ComparisonImage[];
    ornaments?: Ornament[];
}
export interface Document {
    panels: Panel[];
}
export interface TemplateLocks {
    edit: boolean;
    transform: boolean;
    zIndex: boolean;
    overrides?: ImageOverrides | TextFieldOverrides | ShapeOverrides | PositionOverrides;
}
export declare type OverrideType = ImageOverrides | TextFieldOverrides | ShapeOverrides | PositionOverrides;
export interface PositionOverrides {
    sizeAndPosition?: boolean;
}
export interface ShapeOverrides extends PositionOverrides {
    fillColor?: boolean;
    strokeColor?: boolean;
    strokeWidth?: boolean;
}
export interface ImageOverrides extends PositionOverrides {
    crop?: boolean;
    sharpen?: boolean;
}
export interface TextFieldOverrides extends PositionOverrides {
    content?: boolean;
    fontSize?: boolean;
    fontColor?: boolean;
    fontType?: boolean;
}
export interface LockTemplateItem {
    id: string;
    locks: TemplateLocks;
}
export interface PlaceholderImage {
    id: string;
    placeholder: boolean;
    placeholderReplaced?: boolean;
    locks: TemplateLocks;
    label?: string;
}
export interface GenericTemplateItem {
    id: string;
}
export interface BackgroundItem {
    id: string;
    background: boolean;
    locks: TemplateLocks;
}
export interface TemplateTextItem {
    id: string;
    label?: string;
    placeholder?: string;
    locks: TemplateLocks;
}
export declare type TemplateItem =
    | GenericTemplateItem
    | PlaceholderImage
    | LockTemplateItem
    | BackgroundItem
    | TemplateTextItem;
export interface PanelLayoutMapping {
    [key: string]: {
        layoutId: string;
    };
}
export interface DclMetadataItem {
    id: string;
    transforms?: {
        rotatable: boolean;
        scalable: boolean;
    };
    mimeType?: String;
    naturalDimensions?: Dimensions;
    requestId?: String;
    imageTransformations?: {
        appliedProcesses: String[];
    };
    initialSize?: {
        width: number;
        height: number;
    };
    autoPlaced?: boolean;
    isAspectRatioLocked?: boolean;
}
export interface CimDocMetadata {
    [key: string]: any;
    template?: TemplateItem[];
    dsPanelLayout?: PanelLayoutMapping;
    dclMetadata?: DclMetadataItem[];
}
export interface CimDoc {
    version: string;
    document: Document;
    sku?: string;
    fontRepositoryUrl: string;
    metadata?: CimDocMetadata;
    projectionId?: string;
    documentId?: string;
    deleted?: boolean;
    revision?: number;
    owner?: string;
    tenant?: string;
}
