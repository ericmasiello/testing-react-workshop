import { CimDoc } from '../types/CimDoc';

const cimDoc = {
    documentId: '05a7b7cc-5583-45f9-a4b3-a8fd5e3f7297',
    version: '2',
    deleted: false,
    revision: 3,
    owner: 'S1IDG5GJUH4SW3Q346VOX6KK0OT50EO5',
    tenant: 'account:4HVsAccqLzE6BPbmvrDaKw',
    document: {
        panels: [
            {
                id: '7b173fd1-e06a-4a9b-bf43-592758708821',
                name: 'Front',
                width: '88mm',
                height: '58mm',
                images: [
                    {
                        id: '3351cdcd-1a63-467a-a97c-844bb541b27d_vpls_img_1',
                        printUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/68851285-d6f2-424c-af8f-9803f49e7ee7~120/original/?tenant=vbu-cl&vpid=2797771',
                        previewUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/8e1bff93-a7d9-402a-85bd-1ca86b773097~110/original/?tenant=vbu-cl&vpid=2797771',
                        zIndex: 200,
                        position: {
                            x: '0mm',
                            y: '0mm',
                            width: '88.01805555555555mm',
                            height: '58.03194444444444mm',
                        },
                        horizontalAlignment: 'left',
                        verticalAlignment: 'top',
                        pageNumber: 1,
                    },
                ],
                textAreas: [
                    {
                        id: '631c13d4-326c-4153-bee6-ffc4c9768818_vpls_text_3',
                        position: {
                            x: '4.956527777777778mm',
                            y: '33.23096181666667mm',
                            width: '78.105mm',
                            height: '4.6863mm',
                        },
                        horizontalAlignment: 'center',
                        verticalAlignment: 'top',
                        curveAlignment: 0,
                        blockFlowDirection: 'horizontal-tb',
                        textFields: [
                            {
                                fontSize: '12pt',
                                fontStyle: 'normal',
                                fontFamily: 'Mostra Nuova',
                                content: 'Victor Jimenez',
                                color: 'rgb(#FFFFFF)',
                                inlineBaseDirection: 'ltr',
                            },
                        ],
                        textOrientation: 'natural',
                        zIndex: 500,
                    },
                ],
                itemReferences: [
                    {
                        id: 'bfbb017b-886a-4d69-9478-7d7f0699e335_vpls_shape_4',
                        type: 'Word Art',
                        url: 'https://udsinterop.document.vpsvc.com/api/itemref/wordart',
                        data: {
                            fontFamily: 'Calla Web Titling',
                            fontStyle: 'normal,normal',
                            content: 'V',
                            color: 'rgb(255,255,255)',
                            focus: 'center',
                            curve: {
                                radius: 0,
                                height: 0,
                                angle: 0,
                            },
                        },
                        zIndex: 300,
                        position: {
                            x: '4.956527777777778mm',
                            y: '16.428861111111107mm',
                            width: '78.105mm',
                            height: '14.957777777777778mm',
                        },
                    },
                ],
                decorationTechnology: 'offsetOrDigital',
                colorMode: 'color',
            },
            {
                id: '2e0e274b-1045-4e55-8183-68c4868ae01a',
                name: 'Back',
                width: '88mm',
                height: '58mm',
                images: [
                    {
                        id: 'e9a712ca-a98c-419a-9da0-d9523001cb2a_vpls_img_2',
                        printUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/4045969f-dc55-49c8-8120-bebfe6b46f4a~110/original/?tenant=vbu-cl&vpid=2797662',
                        previewUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/3f7188f2-afc3-4bca-b2d6-008eaa02755f~110/original/?tenant=vbu-cl&vpid=2797662',
                        zIndex: 200,
                        position: {
                            x: '0mm',
                            y: '0mm',
                            width: '88.01805555555555mm',
                            height: '58.03194444444444mm',
                        },
                        horizontalAlignment: 'left',
                        verticalAlignment: 'top',
                        pageNumber: 1,
                    },
                    {
                        id: '184d7189-e363-4011-912e-c3a45ae802b6_vpls_img_1',
                        printUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/618a8620-9975-4ef0-8fbf-3d0fc3bf7827~100/original/?tenant=vbu-cl&vpid=2797663',
                        previewUrl:
                            'https://uploads.documents.cimpress.io/v1/uploads/6c92c9ba-5668-4dec-b6e6-7a2c2a69de3a~120/original/?tenant=vbu-cl&vpid=2797663',
                        zIndex: 300,
                        position: {
                            x: '43.16089249722222mm',
                            y: '36.24497943888888mm',
                            width: '1.696270561111109mm',
                            height: '1.696270561111109mm',
                        },
                        horizontalAlignment: 'left',
                        verticalAlignment: 'top',
                        pageNumber: 1,
                    },
                ],
                textAreas: [
                    {
                        id: '09a86145-c32c-42f8-91fd-35a95660bb8c_vpls_text_3',
                        position: {
                            x: '5.274027777777777mm',
                            y: '40.18632777777778mm',
                            width: '37.059305555555554mm',
                            height: '3.1242mm',
                        },
                        horizontalAlignment: 'right',
                        verticalAlignment: 'top',
                        blockFlowDirection: 'horizontal-tb',
                        content: [
                            {
                                fontSize: '8pt',
                                fontStyle: 'normal',
                                fontFamily: 'Mostra Nuova',
                                content: '1 Main St',
                                color: 'rgb(#000000)',
                                inlineBaseDirection: 'ltr',
                            },
                        ],
                        textOrientation: 'natural',
                        zIndex: 400,
                    },
                    {
                        id: 'eafc9f3a-33e2-48d9-bfc1-1db91b6b7ff4_vpls_text_5',
                        position: {
                            x: '45.68472222222222mm',
                            y: '40.18632777777778mm',
                            width: '37.059305555555554mm',
                            height: '3.1242mm',
                        },
                        horizontalAlignment: 'left',
                        verticalAlignment: 'top',
                        blockFlowDirection: 'horizontal-tb',
                        content: [
                            {
                                fontSize: '8pt',
                                fontStyle: 'normal',
                                fontFamily: 'Mostra Nuova',
                                content: 'New York, NY 27001',
                                color: 'rgb(#000000)',
                                inlineBaseDirection: 'ltr',
                            },
                        ],
                        textOrientation: 'natural',
                        zIndex: 600,
                    },
                    {
                        id: '599184ae-c983-4be5-89fa-37277a705ae3_vpls_text_6',
                        position: {
                            x: '14.56972222222222mm',
                            y: '49.469675352777784mm',
                            width: '59.266666666666666mm',
                            height: '3.1242mm',
                        },
                        horizontalAlignment: 'center',
                        verticalAlignment: 'top',
                        blockFlowDirection: 'horizontal-tb',
                        content: [
                            {
                                fontSize: '8pt',
                                fontStyle: 'normal',
                                fontFamily: 'Mostra Nuova',
                                content: '914 789 1200',
                                color: 'rgb(#000000)',
                                inlineBaseDirection: 'ltr',
                            },
                        ],
                        textOrientation: 'natural',
                        zIndex: 700,
                    },
                    {
                        id: '04061b49-b9c0-4901-8b5c-da099d6bafcc_vpls_text_7',
                        position: {
                            x: '14.56972222222222mm',
                            y: '44.66484166666666mm',
                            width: '59.266666666666666mm',
                            height: '3.1242mm',
                        },
                        horizontalAlignment: 'center',
                        verticalAlignment: 'top',
                        blockFlowDirection: 'horizontal-tb',
                        content: [
                            {
                                fontSize: '8pt',
                                fontStyle: 'normal',
                                fontFamily: 'Mostra Nuova',
                                content: 'vj@gmail.com',
                                color: 'rgb(#000000)',
                                inlineBaseDirection: 'ltr',
                            },
                        ],
                        textOrientation: 'natural',
                        zIndex: 800,
                    },
                ],
                decorationTechnology: 'offsetOrDigital',
            },
        ],
    },
    metadata: {
        template: [
            {
                dropdownOptions: [],
                text: 'Victor Jimenez',
                id: '631c13d4-326c-4153-bee6-ffc4c9768818_vpls_text_3',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                    overrides: {},
                },
                placeholder: 'FULL NAME',
                required: false,
                label: 'Full Name',
            },
            {
                id: '3351cdcd-1a63-467a-a97c-844bb541b27d_vpls_img_1',
                placeholder: false,
                locks: {
                    edit: false,
                    transform: true,
                    zIndex: false,
                    overrides: {},
                },
                required: false,
                constraints: {
                    rotation: {},
                    width: {},
                    height: {},
                    canAlterColor: true,
                    canCrop: true,
                },
            },
            {
                id: 'bfbb017b-886a-4d69-9478-7d7f0699e335_vpls_shape_4',
                placeholder: 'H',
                label: 'First Initial',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                },
            },
            {
                id: 'Front',
            },
            {
                dropdownOptions: [],
                text: '1 Main St',
                id: '09a86145-c32c-42f8-91fd-35a95660bb8c_vpls_text_3',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                    overrides: {},
                },
                placeholder: 'address line 1',
                required: false,
                label: 'Address Line 1',
            },
            {
                dropdownOptions: [],
                text: 'New York, NY 27001',
                id: 'eafc9f3a-33e2-48d9-bfc1-1db91b6b7ff4_vpls_text_5',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                    overrides: {},
                },
                placeholder: 'address line 2',
                required: false,
                label: 'Address Line 2',
            },
            {
                dropdownOptions: [],
                text: '914 789 1200',
                id: '599184ae-c983-4be5-89fa-37277a705ae3_vpls_text_6',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                    overrides: {},
                },
                placeholder: 'phone / other',
                required: false,
                label: 'Phone / Other',
            },
            {
                dropdownOptions: [],
                text: 'vj@gmail.com',
                id: '04061b49-b9c0-4901-8b5c-da099d6bafcc_vpls_text_7',
                locks: {
                    edit: false,
                    transform: false,
                    zIndex: false,
                    overrides: {},
                },
                placeholder: 'email / other',
                required: false,
                label: 'Email / Other',
            },
            {
                id: 'e9a712ca-a98c-419a-9da0-d9523001cb2a_vpls_img_2',
                placeholder: false,
                locks: {
                    edit: false,
                    transform: true,
                    zIndex: false,
                    overrides: {},
                },
                required: false,
                constraints: {
                    rotation: {},
                    width: {},
                    height: {},
                    canAlterColor: true,
                    canCrop: true,
                },
            },
            {
                id: '184d7189-e363-4011-912e-c3a45ae802b6_vpls_img_1',
                placeholder: false,
                locks: {
                    edit: false,
                    transform: true,
                    zIndex: false,
                    overrides: {},
                },
                required: false,
                constraints: {
                    rotation: {},
                    width: {},
                    height: {},
                    canAlterColor: true,
                    canCrop: true,
                },
            },
            {
                id: 'Back',
            },
        ],
        documentSources: {
            panels: [
                {
                    id: '7b173fd1-e06a-4a9b-bf43-592758708821',
                    source: 'TEMPLATE_TOKEN',
                    data: 'c2920029..a0bd0bb1-9801-42f7-93bf-fe35649206a0',
                },
                {
                    id: '2e0e274b-1045-4e55-8183-68c4868ae01a',
                    source: 'TEMPLATE_TOKEN',
                    data: 'c2920013..a0bd0bb1-9801-42f7-93bf-fe35649206a0',
                },
            ],
        },
        surfaceUpsells: [
            {
                upsellOffered: true,
                panelName: 'Back',
                optionName: 'Backside',
                optionValue: 'Color',
                pricing: {
                    Blank: {
                        differentialDiscountPrice: 0,
                        differentialListPrice: 0,
                    },
                    Grey: {
                        differentialDiscountPrice: 2.12,
                        differentialListPrice: 2.12,
                    },
                    Color: {
                        differentialDiscountPrice: 3.23,
                        differentialListPrice: 3.23,
                    },
                },
            },
        ],
        dclMetadata: [
            {
                id: 'templateDocumentReferenceUrl',
            },
            {
                id: '631c13d4-326c-4153-bee6-ffc4c9768818_vpls_text_3',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                ordinal: 2,
            },
            {
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                id: '3351cdcd-1a63-467a-a97c-844bb541b27d_vpls_img_1',
                mimeType: '',
                naturalDimensions: {
                    width: 800,
                    height: 527,
                },
                requestId: '761916ef-1d51-4fe3-9d4f-09c285afdb25',
                imageTransformations: {
                    appliedProcesses: [],
                },
                initialSize: {
                    height: 58.03194444444444,
                    width: 88.01805555555555,
                },
                autoPlaced: false,
            },
            {
                id: 'bfbb017b-886a-4d69-9478-7d7f0699e335_vpls_shape_4',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
            },
            {
                id: '09a86145-c32c-42f8-91fd-35a95660bb8c_vpls_text_3',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                ordinal: 1,
            },
            {
                id: 'eafc9f3a-33e2-48d9-bfc1-1db91b6b7ff4_vpls_text_5',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                ordinal: 3,
            },
            {
                id: '599184ae-c983-4be5-89fa-37277a705ae3_vpls_text_6',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                ordinal: 4,
            },
            {
                id: '04061b49-b9c0-4901-8b5c-da099d6bafcc_vpls_text_7',
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                ordinal: 5,
            },
            {
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                id: 'e9a712ca-a98c-419a-9da0-d9523001cb2a_vpls_img_2',
                mimeType: '',
                naturalDimensions: {
                    width: 800,
                    height: 527,
                },
                requestId: '46edbeda-bc9f-485c-ba2a-2be41bc67478',
                imageTransformations: {
                    appliedProcesses: [],
                },
                initialSize: {
                    height: 58.03194444444444,
                    width: 88.01805555555555,
                },
                autoPlaced: false,
            },
            {
                transforms: {
                    rotatable: true,
                    scalable: true,
                },
                id: '184d7189-e363-4011-912e-c3a45ae802b6_vpls_img_1',
                mimeType: '',
                naturalDimensions: {
                    width: 15,
                    height: 15,
                },
                requestId: 'be6413fa-836e-4106-acbd-e8cfec7d095c',
                imageTransformations: {
                    appliedProcesses: [],
                },
                initialSize: {
                    height: 1.696270561111109,
                    width: 1.696270561111109,
                },
                autoPlaced: false,
            },
        ],
        placeholders: {
            version: '2',
            uploads: [],
            canvases: [],
        },
    },
    fontRepositoryUrl:
        'https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published',
} as CimDoc;

export default cimDoc;
