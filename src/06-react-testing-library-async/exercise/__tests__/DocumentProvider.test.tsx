import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { DocumentProvider, useDocument } from '../DocumentProvider';
import cimDoc from '../__fixtures__/cimdoc';
import { rest } from 'msw';
import { server } from '../../../@config/mswConfig';

/*
 * TODO: Configure @config/mswConfig to return a CimDoc. The CimDoc fixture can be found in
 * 05-react-testing-library-async/exercise/__fixtures__/cimdoc.ts
 *
 * TIP: Look at `fetchDocment.ts` in order to decide what the url pattern should
 * look like when mocking the response for fetching the cimDoc.
 */

type DocumentContextState = ReturnType<typeof useDocument>;
type DocumentChildProps = {
    children: (result: DocumentContextState) => React.ReactElement;
};

/*
 * TIP: Use <DocumentChild /> to make testing your provider easier.
 *
 * This component uses the "render props" pattern. It exposes the data
 * within <DocumentProvider /> as a callback function.
 *
 * Example:
 *
 * <DocumentProvider {...yourTestProps}>
 *  <DocumentChild>
 *      {(documentContextState) => {
 *          const { cimDoc, state, errorMessage} = documentContextState;
 *          return (
 *              <div>
 *                  <p>State: {state}</p>
 *                  <pre>JSON.stringify(cimDoc, null, 2)</pre>
 *                  <p>Error message: {errorMessage}</p>
 *              </div>
 *          );
 *      }}
 *  </DocumentChild>
 * </DocumentProvider>
 */
const DocumentChild = ({ children }: DocumentChildProps) => {
    const result = useDocument();
    return children(result);
};

/*
 * TODO: spyOn console.error, providing it a mock implementation that does nothing to silence the errors
 */

/*
 * TODO: reset the the console error spy before each test case runs
 */

it.todo('should provide the state as uninitialized when no CimDoc or docId is provided');

it.todo('should immediately return the cimDoc in a ready state with the cimDoc when passed as a prop');

it.todo('should go into a loading state when fetching the cimDoc');

it.todo('should fetch the document after loading');

/*
 * NOTE: this test requires you mock the response from https://storage.documents.cimpress.io/v3/documents/*
 * so that you can test the error case
 */
it.todo('should set the error state when failing to fetch the document');

/*
 * TIP: try using waitFor() here
 */
it.todo('should log an error when failing to fetch a document');
