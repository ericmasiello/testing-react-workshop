import React from 'react';

type Props = {
    setPokemonName: (name: string) => void;
};

export const PokemonForm = (props: Props) => {
    const { setPokemonName } = props;
    return (
        <form
            onSubmit={event => {
                event.preventDefault();
                const result = new FormData(event.currentTarget).get('pokemonName');
                if (typeof result === 'string') {
                    setPokemonName(result);
                }
            }}
        >
            <label htmlFor="pokemonName">Pokemon Name</label>
            <input type="text" id="pokemonName" name="pokemonName" />
            <button type="submit">Submit</button>
        </form>
    );
};
