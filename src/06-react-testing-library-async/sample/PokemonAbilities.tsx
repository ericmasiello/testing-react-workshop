import React from 'react';

type Props = {
    name: string;
    abilities: string[];
    onReset: () => void;
};

export const PokemonAbilities = (props: Props) => {
    const { name, abilities, onReset } = props;
    return (
        <div>
            <h1>{name} abilities</h1>
            <button onClick={onReset}>Reset</button>
            <ul>
                {abilities.map(ability => (
                    <li key={ability}>{ability}</li>
                ))}
            </ul>
        </div>
    );
};
