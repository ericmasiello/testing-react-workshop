import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { App } from '../App';

it('should initially render a form to input the pokemon name', () => {
    render(<App />);

    // use the <input /> as a proxy to know we have rendered the form
    const input = screen.getByRole('textbox');

    expect(input).toBeDefined();
});

it('should render a loading spinner after submitting the form', async () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    await userEvent.type(pokemonField, 'pikachu{enter}');

    // find and verify the loading indicator is present via its role
    const loader = screen.getByRole('status');
    expect(loader).toBeDefined();
});

it('should render the results after the data is fetched (using waitFor())', async () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    await userEvent.type(pokemonField, 'pikachu{enter}');

    // verify the loading indicator is no present
    screen.getByRole('status');

    // verify all the resultin data is available
    await waitFor(() => {
        const title = screen.getByText(/pikachu abilities/i);
        const firstAbility = screen.getByText(/static/i);
        const secondAbility = screen.getByText(/lightning-rod/i);

        expect(title).toBeDefined();
        expect(firstAbility).toBeDefined();
        expect(secondAbility).toBeDefined();
    });
});

it('should render the results after the data is fetched (using findBy*())', async () => {
    render(<App />);

    const pokemonField = screen.getByLabelText(/pokemon name/i);

    // type pikachu into the field and submit the form via the enter key
    await userEvent.type(pokemonField, 'pikachu{enter}');

    // verify the loading indicator is no present
    screen.getByRole('status');

    // verify all the resultin data is available
    const title = await screen.findByText(/pikachu abilities/i);
    const firstAbility = screen.getByText(/static/i);
    const secondAbility = screen.getByText(/lightning-rod/i);

    expect(title).toBeDefined();
    expect(firstAbility).toBeDefined();
    expect(secondAbility).toBeDefined();
});
