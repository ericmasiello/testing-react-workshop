import React from 'react';

export const Loader = () => <div role="status">Loading...</div>;
