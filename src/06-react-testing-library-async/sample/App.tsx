import React, { useState, useEffect } from 'react';
import { fetchPokemonAbilities } from '../../03-http-fetch-mocks/exercise/pokemonHelpers';
import { Loader } from './Loader';
import { PokemonAbilities } from './PokemonAbilities';
import { PokemonForm } from './PokemonForm';

export const App = () => {
    const [pokemonName, setPokemonName] = useState('');
    const [pokemonAbilities, setPokemonAbilities] = useState<string[] | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        let isMounted = true;
        const execute = async () => {
            setIsLoading(true);
            const abilities = await fetchPokemonAbilities(pokemonName);
            if (isMounted) {
                setPokemonAbilities(abilities);
                setIsLoading(false);
            }
        };
        if (pokemonName) {
            execute();
        }
        return () => {
            isMounted = false;
        };
    }, [pokemonName]);

    if (isLoading) {
        return <Loader />;
    } else if (pokemonAbilities?.length) {
        return (
            <PokemonAbilities
                name={pokemonName}
                abilities={pokemonAbilities}
                onReset={() => {
                    setPokemonName('');
                    setPokemonAbilities(null);
                }}
            />
        );
    } else {
        return <PokemonForm setPokemonName={setPokemonName} />;
    }
};
