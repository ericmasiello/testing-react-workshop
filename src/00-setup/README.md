# Setting up Jest

These are the dependencies you'll need to work with Jest, MSW, and React Testing Library with Typescript.

```bash
yarn add -D tsdx @types/jest jest jest-fetch-mock msw @testing-library/jest-dom @testing-library/react @testing-library/react-hooks identity-obj-proxy
```

Jest automatically installs `babel-jest` and will look for a Babel configuration in your project (e.g. `.babelrc.js`). Make sure your Babel configuration is setup to support Typescript, React, and and other special language features you need.

> **NOTE:** I actually use `tsdx` to as a lightweight wrapper over running Jest. It injects a lot of the configuration necessary for transforming tests written in Typescript to run properly. You can configure itself but you'll need to review the documentation on how to do so.

```json
"scripts": {
    "test": "tsdx test --passWithNoTests"
}
```

## Jest config

I recommend creating a `jest.config.js` file in the root of your project. You will likely want your Jest configuration to include the following:

```javascript
module.exports = {
    moduleNameMapper: {
        // effectively ignores any css imports within modules
        '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
    },
    // specifies a setup file that is invoked before each test run.
    setupFilesAfterEnv: ['./src/@config/jest.ts'],
};
```

You may also specify things like which directories to ignore when running tests, test coverage thresholds, etc.

```javascript
module.exports = {
    moduleNameMapper: {
        // effectively ignores any css imports within modules
        '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
    },
    // specifies a setup file that is invoked before each test run.
    setupFilesAfterEnv: ['./src/@config/jest.ts'],
    coverageThreshold: {
        global: {
            branches: 100,
            functions: 100,
            lines: 100,
            statements: 100,
        },
    },
    collectCoverageFrom: [
        'src/**/*.{js,jsx,ts,tsx}',
        // NOTE: lines that begin with a `!` are a way of saying DO NOT include in coverage
        '!src/**/index.{js,jsx,ts,tsx}',
        '!src/__fixtures__/**/*',
        '!src/**/__stories__/**/*',
        '!src/@types/**/*',
        '!src/@config/**/*',
        '!src/**/*.stories.{js,jsx,ts,tsx}',
    ],
    // specify any custom environment globals
    globals: {
        // __DEV__ is provided by tsdx.
        // See https://tsdx.io/optimization#advanced-code-babel-plugin-dev-expressions-code
        __DEV__: true,
    },
    // ignore any files that match the test pattern that live in the `/dist` directory
    modulePathIgnorePatterns: ['<rootDir>/dist/'],
};
```

### Plugins

I highly recommend installing the `jest-watch-typeahead` plugin. When running Jest in watch mode, this plugin will provide autocomplete behavior for filtering on test files and test cases.

> **NOTE:** This is installed automatically with `tsdx`.

```bash
npm i -D jest-watch-typeahead
```

```bash
yarn add -D jest-watch-typeahead
```

Then, add it to your `jest.config.js` file like so:

```javascript
module.exports = {
    // the rest of your jest config...
    watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
};
```

## Setup file

Your `src/@config/jest.ts` will include anything you need for the Jest environment to be configured correctly.

💡 Note: there is nothing special about the "`@config/`" folder name. Using folders with `@` in front of them for special configurations such as tests (`@config/`) or Typescript types (`@types/`) is a convention I like to follow.

```ts
// sets up JS DOM with values needed by React Testing Library
import '@testing-library/jest-dom';

// polyfills window.fetch
import 'jest-fetch-mock';
```

If you are using MSW (Mock Service Worker), this is also an appropriate place to configure that.

```ts
import '@testing-library/jest-dom';
import 'jest-fetch-mock';
import { server } from './mswConfig';

// Establish API mocking before all tests.
beforeAll(() => server.listen());

// Reset any request handlers that we may add during the tests,
// so they don't affect other tests.
afterEach(() => server.resetHandlers());

// Clean up after the tests are finished.
afterAll(() => server.close());
```
