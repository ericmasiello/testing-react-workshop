import { v4 as uuid } from 'uuid';

export const createEmployees = (names: string[]) => {
    if (names.length === 0) {
        throw new Error('Must pass in one more more employee names');
    }

    return names.map(name => ({
        id: uuid(),
        name,
        startDate: new Date(),
        randoProperty: 'doesnt matter',
    }));
};
