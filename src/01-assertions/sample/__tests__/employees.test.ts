import { createEmployees } from '../employees';

it('should generate employee objects from names', () => {
    const result = createEmployees(['Eric', 'Christina']);

    expect(result).toHaveLength(2);

    const [eric, christina] = result;

    expect(eric).toMatchObject({
        id: expect.any(String),
        name: 'Eric',
        startDate: expect.any(Date),
    });

    expect(christina).toMatchObject({
        id: expect.any(String),
        name: 'Christina',
        startDate: expect.any(Date),
    });
});

it('should throw an error when employee name list is empty', () => {
    // note: you must wrap the function in an extra function else it won't catch the error properly
    expect(() => createEmployees([])).toThrow();
});
