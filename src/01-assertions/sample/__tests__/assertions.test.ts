const add = (a: number, b: number) => a + b;

it('should add two numbers', () => {
    // arrange
    const a = 2;
    const b = 3;

    // act
    const result = add(a, b);

    // assert
    expect(result).toBe(5);
});

it('should match the shape of an object', () => {
    const employee = { name: 'Eric', favoriteColor: 'gray' };
    const sameEmployee = employee;

    expect(employee).toBe(sameEmployee);
    expect(employee).not.toBe({ name: 'Eric', favoriteColor: 'gray' });
    expect(employee).toEqual({ name: 'Eric', favoriteColor: 'gray' });
    expect(employee).toEqual({ name: 'Eric', favoriteColor: 'gray', foo: undefined });
    expect(employee).not.toStrictEqual({ name: 'Eric', favoriteColor: 'gray', foo: undefined });
});

it.todo('should match length using .toHaveLength()');
