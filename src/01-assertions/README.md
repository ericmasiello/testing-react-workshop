# Structuring Tests and Jest Assertions

## Defining a test module

Out of the box, Jest expects your test files to be named `*.test.*` or `*.spec.*`. It does not care what folder these files are in.

-   You can modify this configuration in your project's `jest.config.js`

-   Typically you will see people putting test files next to the file you are testing or in a sub directory called `__tests__`

```
# 🆗 this is okay but you can end up with pretty long lists of files
src/
  employee.ts
  employee.test.ts
 	model.ts
  model.test.ts
```

Or

```
# ✅ I prefer this as it keeps the tests close but also tucked away
src/
  __tests__/
    employee.test.ts
    model.test.s
  employee.ts
  model.ts
```

💡 My preference is to put it in a `__tests__` folder. This keeps the folders a bit tidier, particularly when you have other types of files you want to co-locate such as CSS.

### 💡 Do not mirror your `src/` folder

The `__tests__` **should not** be a mirror of your `src/` folder.

```
# AVOID THIS! ❌❌❌

src/
  folder/
    subfolder/
      someModule.ts
    anotherModule.ts
  employee.ts

 tests/
  folder/
    subfolder/
      someModule.test.ts
    anotherModule.test.ts
  employee.test.ts
```

Instead, create several sub directories adjacent to the files you are testing all named `__tests__`:

```
# Do this! ✅✅✅

src/
  __tests__/
    employee.test.ts
  folder/
    __tests__/
      anotherModule.test.ts
    subfolder/
      __tests__/
        someModule.test.ts
      someModule.ts
    anotherModule.ts
  employee.ts
```

Two primary reasons why the latter is better:

1.  It "mostly" co-locates your tests with the thing you are testing, making it easy to spot whether tests exist at all

2.  It avoids the issue of `import` statements that look like this:

```typescript
// ❌
import thingImTesting from '../../../../../../../src/folder/subfolder/thingImTesting';
```

Instead your imports typically look like this:

```typescript
// ✅
import thingImTesting from '../thingImTesting';
```

## Structuring your tests

### `it` and `test`

Tests can be written using one of two functions, `it` or `test`. They are aliases to one another so it does not matter which you use. **Just stick with one.**

-   💡 My recommendation:

    ```typescript
    // loosely matches "Arrange/Act/Assert"
    it('should [do something] when [condition is true]', () => {
        // test code goes here
    });
    ```

-   💡 Recommended when drafting what tests you want to write:

    ```typescript
    // ✅ You must omit the callback function
    it.todo('should [do something] when [I get around to writing the actual test]');
    ```

-   Not recommended things you _can_ do:

    ```typescript
    // ❌ this will skip running the test.
    it.skip('...', () => {
        /* test code */
    });

    // ❌ this will run only this test (or other tests with .only() on them)
    it.only('...', () => {
        /* test code */
    });
    ```

    💡 I do not recommend using `it.skip` and `it.only`.

    -   If your intent is to only run certain tests, there is a better way using the Jest CLI (see below).
    -   If you're adding `.skip()` to avoid running the test permanently, then remove the test altogether.

### `describe`

`describe` blocks can be used to group related tests.

For most test modules, you typically are testing a single entity (e.g. a React component) so there is no need to wrap the code in a `describe` block as all the `it('...')`s relate to the same entity.

```typescript
import { SubmitButton } from '../SubmitButton';

// ❌ The wrapping describe() block is unnecessary as all tests relate to the same SubmitButton component
describe('SubmitButton', () => {
    it('should render children', () => {
        // ...
    });

    it('should disable the button after clicked', () => {
        // ...
    });
});
```

#### When to use `describe`

If testing a "utility" module that exports multiple simple functions, wrapping related tests in `describe` blocks can aid in organization

```typescript
import { add, subtract } from '../iAmTheBestAtTheMath';

// ✅ The wrapping describe() blocks here are acceptable since we're testing multiple functions from the same module
describe('add', () => {
    it('should add two numbers', () => {
        //...
    });

    it('should [do something else related to addition]', () => {
        // ...
    });
});

describe('subtract', () => {
    it('should subtract two numbers', () => {
        //...
    });

    it('should [do something else related to subtraction]', () => {
        // ...
    });
});
```

If you need to use `beforeAll`, `beforeEach`, `afterEach`, and `afterAll` on a subset of test blocks. This is common when you need to test different behaviors of a component and each differing behavior requires mocking dependencies in different ways.

```typescript
import { App } from '../App';
import * as Visage from '@vp/visage';

const useScreenClassSpy = jest.spyOn(Visage, 'useScreenClass');

// ✅ Here the wrapping `describe` block scope the `beforeEach` and `beforeAll` functions that should run before each `it` block within the `describe` callback.
describe('When mobile', () => {
    beforeAll(() => {
        // mock calls to Visage's useScreenClass to return 'xs' (i.e. "mobile")
        useScreenClassSpy.mockReturnValue('xs');
    });

    beforeEach(() => {
        // clear any counters used by the mock
        useScreenClassSpy.mockClear();
    });

    it('should render as a modal', () => {
        // ...
    });
});

describe('When larger than mobile', () => {
    beforeAll(() => {
        // mock calls to Visage's useScreenClass to return 'md' (i.e. larger than mobile)
        useScreenClassSpy.mockReturnValue('md');
    });

    beforeEach(() => {
        // clear any counters used by the mock
        useScreenClassSpy.mockClear();
    });

    it('should render as a side panel', () => {
        // ...
    });
});
```

## Common Assertions

This list represents assertions I use often _or_ can be easily confused by other types of assertions and warrant further explanation.

### `toBe()` vs. `toEqual()` vs. `toStrictEqual()`

`toBe` does a comparison using `Object.is` which will check for referential equality.

-   This can be used for primitive values

-   I typically use this when either comparing primitive values _or_ if I actually care that the object I am comparing to is the _same_ object in memory and not one that happens to have the same shape

    ```typescript
    expect('foo').toBe('foo'); // true!
    expect(4).toBe(4); // true!

    const fooBar = { foo: 'bar' };
    expect(fooBar).not.toBe({ foo: 'bar' }); // note the use of `not.`
    ```

`toEqual` does a _deep_ compare of values.

-   It acts the same as `toBe` when used on primitive values

-   On non-primitive values (objects, arrays, etc.), it will recursively compare each property

-   **Does not check** for referential equality:

    ```typescript
    expect({ foo: 'bar' }).toEqual({ foo: 'bar' }); // true!
    ```

`toStrictEqual` is similar to `toEqual` except it will go a step further and verify that things like missing properties are not compared to `undefined`. It will also compares sparse arrays similarly.

```typescript
expect({ foo: 'bar', baz: undefined }).toEqual({ foo: 'bar' }); // no problem with toEqual
expect({ foo: 'bar', baz: undefined }).not.toStrictEqual({ foo: 'bar' }); // verifies against missing baz
```

### `toMatchObject()` and `expect.any()`

`toMatchObject` is useful when you want to compare only certain properties of an object _or_ you want to compare that an object has properties of a certain type (but do not care about the value itself).

-   Useful for large objects where you're only interested observing changes to parts of the object

-   Useful for objects that create non-deterministic data such as `new Date()` or create a `uuid`

    ```typescript
    const eric = {
        id: uuid(),
        name: 'Eric',
        startDate: new Date(),
        randoProperty: 'doesnt matter',
    };

    expect(eric).toMatchObject({
        id: expect.any(String), // any type of string is valid here
        name: 'Eric', // matches exactly the literal 'Eric'
        startDate: expect.any(Date), // any type of Date object is value here
        // we don't other making any assertions related to `randoProperty` (i.e. partial object comparison)
    });
    ```

#### `toHaveLength()`

This is purely a convenience for checking any object with an `.length` property (typically arrays).

```typescript
expect([1, 2, 3].length).toBe(3); // manually checking the .length property

expect([1, 2, 3]).toHaveLength(3); // or use toHaveLength()
```

#### `toThrow()`

Useful for testing that a function throws an error. Note that it must be wrapped in an extra function in order to work.

```typescript
expect(createEmployees([]).toThrow(); // will NOT work
expect(() => createEmployees([])).toThrow(); // this works (note the extra function wrapper)
```

I find this most useful when testing React components that use hooks that throw errors when not wrapped in the appropriate provider.

```tsx
import { render } from '@testing-library/react';

it('should throw an error when not wrapped in DataProvider', () => {
    // Assume `ComponentThatShouldBeWrappedInADataProvider` internally uses a hook called `useDataProvider()`
    // and that `useDataProvider` throws an error if it is not wrapped in the context provider: `<DataProvider />`
    expect(() => render(<ComponentThatShouldBeWrappedInADataProvider />)).toThrowError();

    // vs. wrapped in the appropriate provider should not throw an error
    expect(() =>
        render(
            <DataProvider>
                <ComponentThatShouldBeWrappedInADataProvider />
            </DataProvider>
        )
    ).not.toThrowError();
});
```

**NOTE:** `toThrowError()` will only work if the error is thrown synchronously. If the error is throw asynchronously, `toThrowError()` will not catch the error.

#### `toMatchSnapShot()` and `toMatchInlineSnapShot()`

Snapshots are a bit of touchy subject. Some people swear by them. Some won't touch them with a ten foot poll. Let's first understand what snapshots are:

Given this code:

```ts
import { renderHook } from '@testing-library/react-hooks';

it('should throw an error if used outside of a DocumentProvider', () => {
    const { result } = renderHook(() => useDocument());
    expect(result.error).toMatchSnapshot();
});
```

If this is the first time running this code, it will take whatever the value of `result.error` is and attempt to convert into a serializable value and write it your disk. Its an instant _pass_. The idea is that you as the developer will manually verify the value written to disk matches your expectation and if it does, you commit the snapshot file along with the test.

```
// Jest Snapshot v1, https://goo.gl/fbAQLP

exports[`should throw an error if used outside of a DocumentProvider 1`] = `[Error: useDocument must be used in a child component of DocumentProvider]`;
```

You can even snapshot the result of rendering a React component. However, I don't advise this.

**💡 Only use snapshots to assert against small serializable values that are easy to compare in a merge/pull request and will not change frequently.**

React components may start small but will grow over time. Not to mention, they will likely be touched by many hands. I've seen all too often situations where developers will snapshot the output of a React component. Then another dev comes along, makes a change to the component, and the snapshots no longer match. They aren't sure what to do so they just assume its correct and update the local snapshot to match the latest state. Additionally, as these snapshots grow, they become increasingly difficult to parse in a MR/PR.

One other note about snapshots: they are **not** visual snapshots. They have nothing to do with how a React component visually looks in the browser. It is just a serialization of the HTML, attributes, and text that component emits. If you button was green and then it changed to red, your snapshot isn't going to know that.

##### So when should I use a snapshot?

Again, the key here is _small_ pieces of seraializable content. My go to use case is verify error messages such as those you might `throw` or might print with `console.error`. Its easier to snapshot the short string and assert that it exists than to copy/paste the copy into the result of my test. And, if the text does change, it's not much to compare in a diff.

Also, because I keep my snapshots very small, I prefer to use the `toMatchInlineSnapshot()` function over `toMatchSnapshot()` as it writes the output directly into the test file instead of a separate file.

```ts
// before the test runs the first time:
it('should throw an error when not rendered within a <Preview />', () => {
    const { result } = renderHook(() => usePreviewSelectors());
    expect(result.error).toMatchInlineSnapshot();
});

// after the test runs the first time, it is automatically updated with the snapshot value
it('should throw an error when not rendered within a <Preview />', () => {
    const { result } = renderHook(() => usePreviewSelectors());
    expect(result.error).toMatchInlineSnapshot(`[Error: Component must be wrapped in a Preview]`);
});
```

## Jest Promise Assertions [Bonus]

When working with promises, it can be a little difficult to make certain assertions, particularly for failed cases (e.g. using something like `.toThrow()`).

Jest allows you to chain two useful methods off of your `expect()` call that are useful when you're working with promises:

### `.resolves`

Allows you to unwrap a resolved promise and access the value.

It is recommended you include the `expect.assertions(n)`.

```typescript
it('should return <something>', async () => {
    expect.assertions(1);
    await expect(functionThatReturnsAPromise('somePokemon')).resolves.toBe('whateverTheResultIs');
});
```

For these cases, I find its equally readable to use a traditional structure for your test:

```typescript
it('should return <something>', async () => {
    expect.assertions(1);
    const result = await functionThatReturnsAPromise('somePokemon');

    expect(result).toBe('whateverTheResultIs');
});
```

### `.rejects`

For rejected promises, it can be a bit more awkward to handle these in a "traditonal" sense.

```typescript
/*
 * This works but its a bit verbose and its possible to make get a false positive if you forget to include
 * expect.assertions(1);
 */
it('should throw an error when the API is down', async () => {
    expect.assertions(1);

    try {
        await functionThatReturnsAPromise('whatever');
    } catch (error) {
        expect(error).toBeDefined();
    }
});
```

For these, I personally prefer to use `.rejects`

```typescript
it('should throw an error when the API is down', async () => {
    expect.assertions(1);

    // this asserts that the we our function threw an error resulting in a rejected promise
    await expect(functionThatReturnsAPromise('whatever')).rejects.toThrow();
});
```

For either `resolves` or `rejects`, **DO NOT** forget to include the `await` keyword!

## Running your tests

Add a `script` to your package.json that runs `jest`

```json
{
    "scripts": {
        "test": "jest"
    }
}
```

If you are using `tsdx`, use:

```json
{
    "scripts": {
        "test": "tsdx test"
    }
}
```

Run Jest in watch mode by running `npm t -- --watch` or `yarn test --watch`

-   💡 Avoid adding scripts like `test:watch: "jest --watch"` as it clutters up your `"scripts"` with too many commands making it difficult to make sense of

-   Using watch mode:

    ```
    Watch Usage
     › Press a to run all tests.
     › Press f to run only failed tests.
     › Press q to quit watch mode.
     › Press p to filter by a filename regex pattern.
     › Press t to filter by a test name regex pattern.
     › Press Enter to trigger a test run.
    ```

    -   `a` - default, runs all the tests
    -   `p` - filters by by a filename e.g. `employee.test.ts` will only run tests for that module
    -   `t` - filters by test name e.g. `should throw an error` will run all tests that have that string in the `it()` name/subject

Run coverage reports via `npm t -- --coverage` or`yarn test --coverage`

-   Open the coverage report via `open coverage/lcov-report/index.html`
-   "Coverage driven development"
    -   use this to figure out which code paths you _should_ test next
